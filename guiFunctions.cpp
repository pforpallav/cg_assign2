#include "guiFunctions.h"

// ----------------------------------------------------------
// specialKeys() Callback Function
// ----------------------------------------------------------
void specialKeys( int key, int x, int y ) {
  //  Right arrow - increase rotation by 5 degree
  if (key == GLUT_KEY_RIGHT)
    rotate_y += 5;
 
  //  Left arrow - decrease rotation by 5 degree
  else if (key == GLUT_KEY_LEFT)
    rotate_y -= 5;
 
  else if (key == GLUT_KEY_UP)
    rotate_x += 5;
 
  else if (key == GLUT_KEY_DOWN)
    rotate_x -= 5;
 
  //  Request display update
  glutPostRedisplay();
}

void keys(unsigned char c, int x, int y)
{
  //Switching based on the input
  switch(c){
    case 'b':
        myState->openBox(true);
        break;

    case 'B':
        myState->openBox(false);
        break;

    case 'q':
        myState->bendHead(true);
        break;

    case 'Q':
        myState->bendHead(false);
        break;
  
    case 'w':
        myState->turnHead(true);
        break;

    case 'W':
        myState->turnHead(false);
        break;
  
    case 'r':
        myState->tiltHead(true);
        break;

    case 'R':
        myState->tiltHead(false);
        break;
    
    case 27:
        exit(0);
        break;

    default:
        break;
  }
  //Forcing re-displaying
  glutPostRedisplay();
}
