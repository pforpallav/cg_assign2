#include "drawFunctions.h"
#include "colors.h"

//Function to draw the box with openAng between lid and box
void drawBox(int openAng, float aspRatio){
    //Inner wall
    glPushMatrix();
        glScalef(1.0, 0.5, 1.0);
        glPushMatrix();
            glTranslatef(0.0, 0.1, 0.0);
            glScalef(0.9, 0.9, 0.9);
            drawOpenCuboid(dbrown, white);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0.0, 0.1, 0.0);
            glScalef(0.8, 0.7, 0.8);
            drawOpenCuboid(black, white);
        glPopMatrix();

        //Outer wall
        drawOpenCuboid(brown, white);
        //Border1
        glColor3f(green->R, green->G, green->B);
        glBegin(GL_QUADS);
            glVertex3f(1.0, 1.0, 1.0);
            glVertex3f(1.0, 1.0, -1.0);
            glVertex3f(0.9, 1.0, -1.0);
            glVertex3f(0.9, 1.0, 1.0);
            
            glVertex3f(-1.0, 1.0, -1.0);
            glVertex3f(-1.0, 1.0, 1.0);
            glVertex3f(-0.9, 1.0, 1.0);
            glVertex3f(-0.9, 1.0, -1.0);

            glVertex3f(-0.9, 1.0, 0.9);
            glVertex3f(-0.9, 1.0, 1.0);
            glVertex3f(0.9, 1.0, 1.0);
            glVertex3f(0.9, 1.0, 0.9);

            glVertex3f(0.9, 1.0, -0.9);
            glVertex3f(0.9, 1.0, -1.0);
            glVertex3f(-0.9, 1.0, -1.0);
            glVertex3f(-0.9, 1.0, -0.9);
        glEnd();
        //Border2
        glPushMatrix();
            //glTranslatef(0.0, -0.5, 0.0);
            glColor3f(dgreen->R, dgreen->G, dgreen->B);
            glBegin(GL_QUADS);
                glVertex3f(0.9, 0.8, 0.9);
                glVertex3f(0.9, 0.8, -0.9);
                glVertex3f(0.8, 0.8, -0.9);
                glVertex3f(0.8, 0.8, 0.9);
                
                glVertex3f(-0.9, 0.8, -0.9);
                glVertex3f(-0.9, 0.8, 0.9);
                glVertex3f(-0.8, 0.8, 0.9);
                glVertex3f(-0.8, 0.8, -0.9);

                glVertex3f(-0.8, 0.8, 0.8);
                glVertex3f(-0.8, 0.8, 0.9);
                glVertex3f(0.8, 0.8, 0.9);
                glVertex3f(0.8, 0.8, 0.8);

                glVertex3f(0.8, 0.8, -0.8);
                glVertex3f(0.8, 0.8, -0.9);
                glVertex3f(-0.8, 0.8, -0.9);
                glVertex3f(-0.8, 0.8, -0.8);
            glEnd();
        glPopMatrix();
        //drawBorder(green);
    glPopMatrix();

    //Lid
    glPushMatrix();
        glTranslatef(0.0, 0.5, -1.0);
        glRotatef(360-openAng, 1.0, 0.0, 0.0);
        glTranslatef(0.0, 1.0, 1.0);
        glScalef(1.0, -1.0, 1.0);
        glPushMatrix();
            glTranslatef(0.0, 0.1, 0.0);
            glScalef(0.9, 1, 0.9);
            drawOpenCuboid(dbrown, white);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(0.0, 0.2, 0.0);
            glScalef(0.8, 0.9, 0.8);
            drawOpenCuboid(black, white);
        glPopMatrix();

        //Outer wall
        drawOpenCuboid(brown, white);
        //Border1
        glColor3f(red->R, red->G, red->B);
        glBegin(GL_QUADS);
            glVertex3f(1.0, 1.0, 1.0);
            glVertex3f(1.0, 1.0, -1.0);
            glVertex3f(0.9, 1.0, -1.0);
            glVertex3f(0.9, 1.0, 1.0);
            
            glVertex3f(-1.0, 1.0, -1.0);
            glVertex3f(-1.0, 1.0, 1.0);
            glVertex3f(-0.9, 1.0, 1.0);
            glVertex3f(-0.9, 1.0, -1.0);

            glVertex3f(-0.9, 1.0, 0.9);
            glVertex3f(-0.9, 1.0, 1.0);
            glVertex3f(0.9, 1.0, 1.0);
            glVertex3f(0.9, 1.0, 0.9);

            glVertex3f(0.9, 1.0, -0.9);
            glVertex3f(0.9, 1.0, -1.0);
            glVertex3f(-0.9, 1.0, -1.0);
            glVertex3f(-0.9, 1.0, -0.9);
        glEnd();
        //Border2
        glColor3f(dred->R, dred->G, dred->B);
        glBegin(GL_QUADS);
            glVertex3f(0.9, 1.1, 0.9);
            glVertex3f(0.9, 1.1, -0.9);
            glVertex3f(0.8, 1.1, -0.9);
            glVertex3f(0.8, 1.1, 0.9);
            
            glVertex3f(-0.9, 1.1, -0.9);
            glVertex3f(-0.9, 1.1, 0.9);
            glVertex3f(-0.8, 1.1, 0.9);
            glVertex3f(-0.8, 1.1, -0.9);

            glVertex3f(-0.8, 1.1, 0.8);
            glVertex3f(-0.8, 1.1, 0.9);
            glVertex3f(0.8, 1.1, 0.9);
            glVertex3f(0.8, 1.1, 0.8);

            glVertex3f(0.8, 1.1, -0.8);
            glVertex3f(0.8, 1.1, -0.9);
            glVertex3f(-0.8, 1.1, -0.9);
            glVertex3f(-0.8, 1.1, -0.8);
        glEnd();
        //drawBorder(red);
    glPopMatrix();
    
    //Drawing a scaled down version for the cubiod for inner wall
}

//Function to draw an open cuboid
void drawOpenCuboid(Color* boxColor, Color* edgeColor){
    //Pushing and ensuring popped at the end
    glPushMatrix();
    
    //Inner box 
    glColor3f(boxColor->R, boxColor->G, boxColor->B);
    glBegin(GL_QUADS);
        //Back face
        glVertex3f(1.0, 1.0, -1.0);
        glVertex3f(1.0, -1.0, -1.0);
        glVertex3f(-1.0, -1.0, -1.0);
        glVertex3f(-1.0, 1.0, -1.0);

        //Front face
        glVertex3f(1.0, 1.0, 1.0);
        glVertex3f(1.0, -1.0, 1.0);
        glVertex3f(-1.0, -1.0, 1.0);
        glVertex3f(-1.0, 1.0, 1.0);

        //Side face
        glVertex3f(1.0, 1.0, 1.0);
        glVertex3f(1.0, 1.0, -1.0);
        glVertex3f(1.0, -1.0, -1.0);
        glVertex3f(1.0, -1.0, 1.0);

        //Side face
        glVertex3f(-1.0, 1.0, 1.0);
        glVertex3f(-1.0, 1.0, -1.0);
        glVertex3f(-1.0, -1.0, -1.0);
        glVertex3f(-1.0, -1.0, 1.0);

        //Bottom face
        glVertex3f(1.0, -1.0, 1.0);
        glVertex3f(1.0, -1.0, -1.0);
        glVertex3f(-1.0, -1.0, -1.0);
        glVertex3f(-1.0, -1.0, 1.0);
    glEnd();

    glColor3f(edgeColor->R, edgeColor->G, edgeColor->B);
    glutWireCube(2.0);

    //Re-setting previous value
    glPopMatrix();
}

void drawBorder(Color* borderColor){
    //Strip inbetween
    glColor3f(borderColor->R, borderColor->G, borderColor->B);
    glBegin(GL_QUADS);
        glVertex3f(1.0, 1.0, 1.0);
        glVertex3f(1.0, 1.0, -1.0);
        glVertex3f(0.8, 1.0, -1.0);
        glVertex3f(0.8, 1.0, 1.0);
        
        glVertex3f(-1.0, 1.0, -1.0);
        glVertex3f(-1.0, 1.0, 1.0);
        glVertex3f(-0.8, 1.0, 1.0);
        glVertex3f(-0.8, 1.0, -1.0);

        glVertex3f(-0.8, 1.0, 0.8);
        glVertex3f(-0.8, 1.0, 1.0);
        glVertex3f(0.8, 1.0, 1.0);
        glVertex3f(0.8, 1.0, 0.8);

        glVertex3f(0.8, 1.0, -0.8);
        glVertex3f(0.8, 1.0, -1.0);
        glVertex3f(-0.8, 1.0, -1.0);
        glVertex3f(-0.8, 1.0, -0.8);
    glEnd();
}


//Function to draw the head of the figure
void drawPerson(State* state){

    glTranslatef(0.0, 4.0, 0.0);
    glPushMatrix();
        drawHead(state->headBend, state->headTurn, state->headTilt);
        glTranslatef(0.0, -2.0, 0.0);
        drawJoint(0.5);
        glTranslatef(0.0, -4.0, 0.0);
        drawTorso();

        glPushMatrix();
            glTranslatef(0, -1.6, 0);
            glScalef(0.75, 0.3, 0.75);
            drawTorso();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-2, 4, 0.0);
            glRotatef(-40, 0.0, 0.0, 1.0);
            drawHand(state, true);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(2, 4, 0.0);
            glRotatef(40, 0.0, 0.0, 1.0);
            drawHand(state, false);
        glPopMatrix();
        //drawLimb();
        glTranslatef(1.5, -1.5, 0.0);
        //drawJoint();
        //drawLimb();
        //Hand
    glPopMatrix();
}

void drawHead(int bendAng, int sideAng, int tiltAng){
    glColor3f(brown->R, brown->G, brown->B);

    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glPushMatrix();
        glTranslatef(0.0, -1.2, 0.0);
        glRotatef(bendAng, 1.0, 0.0, 0.0);
        glRotatef(tiltAng, 0.0, 0.0, 1.0);
        glRotatef(sideAng, 0.0, 1.0, 0.0);
        glTranslatef(0.0, 1.2, 0.0);
        glScalef(0.8, 1.2, 0.8);
        gluSphere(qobj2, 1, 20, 20);
        //Just to check the correctness of the model
        #if GRID
            glColor3f(black->R, black->G, black->B);
            gluSphere(qobj, 1, 20, 20);
        #endif
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}

void drawJoint(float jSize){
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBindTexture(GL_TEXTURE_2D, texture[1]);

    glColor3f(brown->R, brown->G, brown->B);
    gluSphere(qobj2, jSize, 20, 20);

    glDisable(GL_TEXTURE_2D);
}

void drawLimb(){
    glColor3f(brown->R, brown->G, brown->B);

    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glPushMatrix();
        glScalef(0.4, 1.5, 0.4);
        gluSphere(qobj2, 1, 20, 20);
        //Just to check the correctness of the model
        #if GRID
            glColor3f(black->R, black->G, black->B);
            gluSphere(qobj, 1, 20, 20);
        #endif
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}

void drawTorso(){
    glColor3f(brown->R, brown->G, brown->B);

    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glPushMatrix();
        glScalef(1, 1, 0.5);
        glPushMatrix();
          glScalef(1, 0.3, 1);
          glColor3f (0.5, 0.2, 0.2);
          gluSphere(qobj2, 1.5, 20, 20);
          #if GRID
            glColor3f(1,1,1);
            gluSphere(qobj, 1.5, 20, 20);
          #endif
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, texture[1]);
        glPushMatrix();
          glRotatef(270, 1, 0, 0);
          #if GRID
            glColor3f(1,1,1);
            gluCylinder(qobj, 2.0, 1.5, 4, 40, 16);
          #endif
          glColor3f (0.5, 0.2, 0.2);
          gluCylinder(qobj2, 1.5, 2.0, 4, 40, 16);
        glPopMatrix();
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        glPushMatrix();
          glTranslatef(0, 4, 0);
          glScalef(1, 0.5, 1);
          glColor3f (0.5, 0.2, 0.2);
          gluSphere(qobj2, 2, 20, 20);
          #if GRID
            glColor3f(1,1,1);
            gluSphere(qobj, 2, 20, 20);
          #endif
        glPopMatrix();
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
}

void drawHand(State* state, bool arm){
    glPushMatrix();
        drawJoint(0.5);
        //Left arm
        if(arm){
            glRotatef(-1*state->lArmFront, 1.0, 0.0, 0.0);
            glRotatef(-1*state->lArmSide, 0.0, 0.0, 1.0);
            glRotatef(-1*state->lArmRot, 0.0, 1.0, 0.0);
        }
        else{
            glRotatef(1*state->rArmFront, 1.0, 0.0, 0.0);
            glRotatef(1*state->rArmSide, 0.0, 0.0, 1.0);
            glRotatef(1*state->rArmRot, 0.0, 1.0, 0.0);
        }
        glTranslatef(0.0, -1.5, 0.0);
        drawLimb();
        glPushMatrix();
            glTranslatef(0.0, -1.5, 0.0);
            drawJoint(0.2);
            glTranslatef(0.0, -1.5, 0.0);
            //Rotating to given angle
            glTranslatef(0.0, 1.5, 0.0);
            if(arm) glRotatef(-1*state->lElbowAng, 1.0, 0.0, 0.0);
            else glRotatef(state->rElbowAng, 1.0, 0.0, 0.0);
            glTranslatef(0.0, -1.5, 0.0);
            drawLimb();
        glPopMatrix();
    glPopMatrix();
}

