#include <iostream>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <vector>

#ifndef PATH
#define PATH

#define noTextures 14

extern GLuint texture[noTextures];
extern GLUquadric* qobj;
extern GLUquadric* qobj2;

//3D point
class Point{
    public: 
        float x, y, z;

        //Constructor
        Point(){}

        Point(float X, float Y, float Z);
        
        //Operational overloads (addition, multiplication)
};

class Path{
    public:
        //List of points choosen by user
        std::vector<Point> cPts;

        //List of last sampled points
        std::vector<Point> lastSamplePts;

        //Constructors
        Path();

        Path(std::vector<Point> controlPoints);

        //Adding a 3D point
        void addPoint(Point newPt);

        //Drawing the control points
        void drawControlPoints();

        //Drawing the given points
        void drawPoints(std::vector<Point> points);

        //Printing all the points
        void printPoints();

        //Creates the sample points for required
        void getSamplePoints(float stepSize);

        //Clear all points
        void clear();

        //Clear undo one step
        void undo();

    private:
        //Implementation of DeCalisjlau's algorithm
    std::vector<Point> recurseLinear(std::vector<Point> curPts, float u);
};













#endif
