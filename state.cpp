#include "state.h"

State::State(){
    //Assign all default values to the states (currently set to one dancing position)
    boxOpenAng = 0;
    
    headTilt = 20;
    headBend = 10;
    headTurn = 30;

    lArmFront = 90;
    lArmSide = 5;
    lArmRot = 0;

    rArmFront = 90;
    rArmSide = -15;
    rArmRot = 30;
    
    rElbowAng = 10;
    lElbowAng = 0;

    torsoBend = 20;
    torsoTurn = -10;
    torsoTilt = 0;

    lLegFront = -15;
    lLegSide = 15;
    lLegRot = 180;

    rLegFront = 15;
    rLegSide = 20;
    rLegRot = 180;

    rKneeAng = 40;
    lKneeAng = 30;

    //Pendulum motion
    pendAng = 20;
    pendMotion = true;

    light = true;
    lamp = true;

    stepSize = 0.15;
    curMove = 0;
    curStep = 0;
}

bool State::openBox(bool type){
    if(type) boxOpenAng = (boxOpenAng < 120)? boxOpenAng + 5 : boxOpenAng; 
    else  boxOpenAng = (boxOpenAng > 0)? boxOpenAng - 5 : boxOpenAng;

    if(boxOpenAng == 120)
        return true;
    else
        return false;
}

void State::bendHead(bool type){
    if(type) headBend = (headBend < 90)? headBend + 5 : headBend; 
    else  headBend = (headBend > 0)? headBend - 5 : headBend; 
}

void State::tiltHead(bool type){
    if(type) headTilt = (headTilt < 60)? headTilt + 5 : headTilt; 
    else  headTilt = (headTilt > -60)? headTilt - 5 : headTilt; 
}

void State::turnHead(bool type){
    if(type) headTurn = (headTurn < 90)? headTurn + 5 : headTurn; 
    else  headTurn = (headTurn > -90)? headTurn - 5 : headTurn; 
}

void State::sideArm(bool arm, bool type){
    //Left arm
    if(arm){
        if(type) lArmSide = (lArmSide < 90)? lArmSide + 5 : lArmSide; 
        else  lArmSide = (lArmSide > -90)? lArmSide - 5 : lArmSide; 
    }
    //Right arm
    else{
        if(type) rArmSide = (rArmSide < 90)? rArmSide + 5 : rArmSide; 
        else  rArmSide = (rArmSide > -90)? rArmSide - 5 : rArmSide; 
    }
}

void State::frontArm(bool arm, bool type){
    //Left arm
    if(arm){
        if(type) lArmFront = (lArmFront < 90)? lArmFront + 5 : lArmFront; 
        else  lArmFront = (lArmFront > -90)? lArmFront - 5 : lArmFront; 
    }
    //Right arm
    else{
        if(type) rArmFront = (rArmFront < 90)? rArmFront + 5 : rArmFront; 
        else  rArmFront = (rArmFront > -90)? rArmFront - 5 : rArmFront; 
    }
}

void State::rotateArm(bool arm, bool type){
    //Left arm
    if(arm){
        if(type) lArmRot = (lArmRot < 90)? lArmRot + 5 : lArmRot; 
        else  lArmRot = (lArmRot > -90)? lArmRot - 5 : lArmRot; 
    }
    //Right arm
    else{
        if(type) rArmRot = (rArmRot < 90)? rArmRot + 5 : rArmRot; 
        else  rArmRot = (rArmRot > -90)? rArmRot - 5 : rArmRot; 
    }
}

void State::bendElbow(bool arm, bool type){
    //Left arm
    if(arm){
        if(type) lElbowAng = (lElbowAng < 180)? lElbowAng + 5 : lElbowAng; 
        else  lElbowAng = (lElbowAng > 10)? lElbowAng - 5 : lElbowAng; 
    }
    //Right arm
    else{
        if(type) rElbowAng = (rElbowAng < 180)? rElbowAng + 5 : rElbowAng; 
        else  rElbowAng = (rElbowAng > 10)? rElbowAng - 5 : rElbowAng; 
    }
}

void State::bendTorso(bool type){
    if(type) torsoBend = (torsoBend < 90)? torsoBend + 5 : torsoBend; 
    else  torsoBend = (torsoBend > 0)? torsoBend - 5 : torsoBend; 
}

void State::turnTorso(bool type){
    if(type) torsoTurn = (torsoTurn < 20)? torsoTurn + 5 : torsoTurn; 
    else  torsoTurn = (torsoTurn > -20)? torsoTurn - 5 : torsoTurn; 
}

void State::tiltTorso(bool type){
    if(type) torsoTilt = (torsoTilt < 20)? torsoTilt + 5 : torsoTilt; 
    else  torsoTilt = (torsoTilt > -20)? torsoTilt - 5 : torsoTilt; 
}

void State::sideLeg(bool leg, bool type){
    //Left arm
    if(leg){
        if(type) lLegSide = (lLegSide < 90)? lLegSide + 5 : lLegSide; 
        else  lLegSide = (lLegSide > -90)? lLegSide - 5 : lLegSide; 
    }
    //Right arm
    else{
        if(type) rLegSide = (rLegSide < 90)? rLegSide + 5 : rLegSide; 
        else  rLegSide = (rLegSide > -90)? rLegSide - 5 : rLegSide; 
    }
}

void State::frontLeg(bool leg, bool type){
    //Left leg
    if(leg){
        if(type) lLegFront = (lLegFront < 90)? lLegFront + 5 : lLegFront; 
        else  lLegFront = (lLegFront > -90)? lLegFront - 5 : lLegFront; 
    }
    //Right leg
    else{
        if(type) rLegFront = (rLegFront < 90)? rLegFront + 5 : rLegFront; 
        else  rLegFront = (rLegFront > -90)? rLegFront - 5 : rLegFront; 
    }
}

void State::rotateLeg(bool leg, bool type){
    //Left leg
    if(leg){
        if(type) lLegRot = (lLegRot < 270)? lLegRot + 5 : lLegRot; 
        else  lLegRot = (lLegRot > 90)? lLegRot - 5 : lLegRot; 
    }
    //Right leg
    else{
        if(type) rLegRot = (rLegRot < 270)? rLegRot + 5 : rLegRot; 
        else  rLegRot = (rLegRot > 90)? rLegRot - 5 : rLegRot; 
    }
}

void State::bendKnee(bool leg, bool type){
    //Left leg
    if(leg){
        if(type) lKneeAng = (lKneeAng < 180)? lKneeAng + 5 : lKneeAng; 
        else  lKneeAng = (lKneeAng > 0)? lKneeAng - 5 : lKneeAng; 
    }
    //Right leg
    else{
        if(type) rKneeAng = (rKneeAng < 180)? rKneeAng + 5 : rKneeAng; 
        else  rKneeAng = (rKneeAng > 0)? rKneeAng - 5 : rKneeAng; 
    }
}

void State::updateTime(){
    if(pendMotion){
        if(pendAng < 20){
            pendAng += 2;
        }
        else{
            pendAng -= 2;
            pendMotion = !pendMotion;
        }
    }
    else{
        if(pendAng > -20){
            pendAng -= 2;
        }
        else{
            pendAng += 2;
            pendMotion = !pendMotion;
        }
    }
}

void State::rotateFan(){
    fanAng = (fanAng < 360)? fanAng + 20 : 0;
}

void State::openDoor(bool type){
    if(type) doorOpen = (doorOpen < 20)? doorOpen + 1 : doorOpen;
    if(!type) doorOpen = (doorOpen > 0)? doorOpen - 1 : doorOpen;
}

void State::printStates(){
    /*
    //Head DOFs
    printf("Head\n====\n(Bend, Tilt, Turn) : (%d, %d, %d)\n\n", headBend, headTilt, headTurn);
    //Torso DoFs
    printf("Torso\n====\n(Bend, Tilt, Turn) : (%d, %d, %d)\n\n", torsoBend, torsoTilt, torsoTurn);
    //Left Arm DOFs
    printf("Left Arm\n====\n(Front, Side, Rotate) : (%d, %d, %d)\n\n", lArmFront, lArmSide, lArmRot);
    printf("Left Elbow : %d\n", lElbowAng);
    //Right Arm DOFs
    printf("Right Arm\n====\n(Front, Side, Rotate) : (%d, %d, %d)\n\n", rArmFront, rArmSide, rArmRot);
    printf("Right Elbow : %d\n", rElbowAng);
    //Left Leg DOFs
    printf("Left Leg\n====\n(Front, Side, Rotate) : (%d, %d, %d)\n\n", lLegFront, lLegSide, lLegRot);
    printf("Left Knee : %d\n", lKneeAng);
    //RIght Leg DOFs
    printf("Right Leg\n====\n(Front, Side, Rotate) : (%d, %d, %d)\n\n", rLegFront, rLegSide, rLegRot);
    printf("Right Knee : %d\n", rKneeAng);
    */
    std::cout << headTilt << "\t" << headBend << "\t" << headTurn << "\t" << lArmFront << "\t" << lArmSide << "\t" << lArmRot << "\t" << rArmFront << "\t" << rArmSide << "\t" << rArmRot << "\t" << rElbowAng << "\t" << lElbowAng << "\t" << rLegFront << "\t" << rLegSide << "\t" << rLegRot << "\t" << lLegFront << "\t" << lLegSide << "\t" << lLegRot << "\t" << rKneeAng << "\t" << lKneeAng << "\t" << torsoBend << "\t" << torsoTurn << "\t" << torsoTilt << std::endl;
}

void State::clearState(){
    moveList.clear();
    moveList.push_back(saveStateToPose());
    return;
}

void State::addState(){
    moveList.push_back(saveStateToPose());
}

void State::loadStateFromPose(Pose pose){
    //Head
    headTilt = pose.headTilt;
    headBend = pose.headBend;
    headTurn = pose.headTurn;

    //Left Arm Joint
    lArmFront = pose.lArmFront;
    lArmSide = pose.lArmSide; //side angle
    lArmRot = pose.lArmRot; //Rotation of the arm

    //Right Arm Joint
    rArmFront = pose.rArmFront; //front and back angle
    rArmSide = pose.rArmSide; //side angle
    rArmRot = pose.rArmRot; //Rotation of the arm

    //Elbow angle
    rElbowAng = pose.rElbowAng;
    lElbowAng = pose.lElbowAng;

    //Right leg joint
    rLegFront = pose.rLegFront;
    rLegSide = pose.rLegSide;
    rLegRot = pose.rLegRot;

    //Left leg joint
    lLegFront = pose.lLegFront;
    lLegSide = pose.lLegSide;
    lLegRot = pose.lLegRot;

    //Knee angle
    rKneeAng = pose.rKneeAng;
    lKneeAng = pose.lKneeAng;

    //torso angle
    torsoBend = pose.torsoBend; 
    torsoTurn = pose.torsoTurn;
    torsoTilt = pose.torsoTilt;
}

Pose State::saveStateToPose(){
    Pose pose = Pose();
    //Head
    pose.headTilt = headTilt;
    pose.headBend = headBend;
    pose.headTurn = headTurn;

    //Left Arm Joint
    pose.lArmFront = lArmFront;
    pose.lArmSide = lArmSide; //side angle
    pose.lArmRot = lArmRot; //Rotation of the arm

    //Right Arm Joint
    pose.rArmFront = rArmFront; //front and back angle
    pose.rArmSide = rArmSide; //side angle
    pose.rArmRot = rArmRot; //Rotation of the arm

    //Elbow angle
    pose.rElbowAng = rElbowAng;
    pose.lElbowAng = lElbowAng;

    //Right leg joint
    pose.rLegFront = rLegFront;
    pose.rLegSide = rLegSide;
    pose.rLegRot = rLegRot;

    //Left leg joint
    pose.lLegFront = lLegFront;
    pose.lLegSide = lLegSide;
    pose.lLegRot = lLegRot;

    //Knee angle
    pose.rKneeAng = rKneeAng;
    pose.lKneeAng = lKneeAng;

    //torso angle
    pose.torsoBend = torsoBend; 
    pose.torsoTurn = torsoTurn;
    pose.torsoTilt = torsoTilt;

    //Returning Pose
    return pose;
}

void State::animateState(){
    //Going to next move
    if(curStep + stepSize > 1){
        curStep = 0;
        curMove = ((curMove + 1) < moveList.size())? (curMove + 1) : 0; 
    }
    else{
        curStep += stepSize;
    }

    if(curMove < (moveList.size() - 1))
        loadStateFromPose(moveList[curMove + 1]*curStep + moveList[curMove]*(1-curStep));
    else
        loadStateFromPose(moveList[0]*curStep + moveList[curMove]*(1-curStep));

    //std::cout << curStep << " " << curMove << std::endl;
    //printStates();
}

void State::savePoseList(std::string fileName){
    std::ofstream file(fileName.c_str());
    int noPoses = moveList.size();

    //Writing number of poses
    file << noPoses << "\n";
    //Writing all the poses
    for(int i = 0; i < noPoses; i++){
        //Head
        file << moveList[i].headTilt << "\t"; 
        file << moveList[i].headBend << "\t";
        file << moveList[i].headTurn << "\t";

        //Left Arm Joint
        file << moveList[i].lArmFront << "\t";
        file << moveList[i].lArmSide << "\t"; //side angle
        file << moveList[i].lArmRot << "\t"; //Rotation of the arm

        //Right Arm Joint
        file << moveList[i].rArmFront << "\t"; //front and back angle
        file << moveList[i].rArmSide << "\t"; //side angle
        file << moveList[i].rArmRot << "\t"; //Rotation of the arm

        //Elbow angle
        file << moveList[i].rElbowAng << "\t";
        file << moveList[i].lElbowAng << "\t";

        //Right leg joint
        file << moveList[i].rLegFront << "\t";
        file << moveList[i].rLegSide << "\t";
        file << moveList[i].rLegRot << "\t";

        //Left leg joint
        file << moveList[i].lLegFront << "\t";
        file << moveList[i].lLegSide << "\t";
        file << moveList[i].lLegRot << "\t";

        //Knee angle
        file << moveList[i].rKneeAng << "\t";
        file << moveList[i].lKneeAng << "\t";

        //torso angle
        file << moveList[i].torsoBend << "\t"; 
        file << moveList[i].torsoTurn << "\t";
        file << moveList[i].torsoTilt << " \n";
    }
    
    //Closing the file
    file.close();
}

void State::loadPoseList(std::string fileName){
    std::ifstream file(fileName.c_str());

    std::string angValue;
    int noPoses;
    moveList.clear();
    //Reading number of Poses
    file >> angValue;
    noPoses = atoi(angValue.c_str());
    for(int i = 0; i < noPoses; i++){
        //Reading a pose
        Pose newPose = Pose();
        file >> newPose.headTilt;
        
        file >> newPose.headBend;
        file >> newPose.headTurn;

        file >> newPose.lArmFront;
        file >> newPose.lArmSide;
        file >> newPose.lArmRot;

        file >> newPose.rArmFront;
        file >> newPose.rArmSide;
        file >> newPose.rArmRot;
        
        file >> newPose.rElbowAng;
        file >> newPose.lElbowAng;

        file >> newPose.rLegFront;
        file >> newPose.rLegSide;
        file >> newPose.rLegRot;

        file >> newPose.lLegFront;
        file >> newPose.lLegSide;
        file >> newPose.lLegRot;

        file >> newPose.rKneeAng;
        file >> newPose.lKneeAng;

        file >> newPose.torsoBend;
        file >> newPose.torsoTurn;
        file >> newPose.torsoTilt;

        moveList.push_back(newPose);
        //newPose.printPose();
    }

    file.close();
}




