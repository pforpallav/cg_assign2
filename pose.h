#ifndef POSE
#define POSE

#include <stdio.h>
#include <iostream>
#include <vector>

//Class to hold the POSE variable of the model
class Pose{
    public:
        //Pose variables
        //Head
        int headTilt; //Tilt angle 
        int headBend; //Bend front and back
        int headTurn; //Turning sidewards

        //Left Arm Joint
        int lArmFront; //front and back angle
        int lArmSide; //side angle
        int lArmRot; //Rotation of the arm

        //Right Arm Joint
        int rArmFront; //front and back angle
        int rArmSide; //side angle
        int rArmRot; //Rotation of the arm

        //Elbow angle
        int rElbowAng;
        int lElbowAng;

        //Right leg joint
        int rLegFront;
        int rLegSide;
        int rLegRot;

        //Left leg joint
        int lLegFront;
        int lLegSide;
        int lLegRot;

        //Knee angle
        int rKneeAng;
        int lKneeAng;

        //torso angle
        int torsoBend;
        int torsoTurn;
        int torsoTilt;

    //Methods
    Pose(){};
    
    //Methods for easy access
    Pose operator+(Pose oPose);

    Pose operator*(float scalar);

    //Methods for debugging
    void printPose();
};
#endif
