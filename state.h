#ifndef STATE
#define STATE

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "pose.h"

//Class contains all the information and constrains regarding the state of all the angles of DOFs

class State{
    public:
        //Indices for display lists stored in state class
        GLuint boxPartList, 
                boxPartList2,
                limbList,
                jointList,
                torsoList,
                neckList,
                headList,
                hatList,
                stickList,
                wallList,
                floorList,
                ceilingList,
                tableLegList,
                chairLegList,
                chairBackList,
                bookShelfList,
                shoeShelfList,
                clockList,
                fanList;

        //Contains all the moves (intermediate states)
        std::vector<Pose> moveList;
        int curMove;
        float stepSize;
        float curStep;

        //List of all angles that can be used by the drawing functions
        //All angles are measure wrt torso or next level highest part
        //Box
        int boxOpenAng; //Opening angle of the box
        int doorOpen; //Opening translation of the door

        //Head
        int headTilt; //Tilt angle 
        int headBend; //Bend front and back
        int headTurn; //Turning sidewards

        //Left Arm Joint
        int lArmFront; //front and back angle
        int lArmSide; //side angle
        int lArmRot; //Rotation of the arm

        //Right Arm Joint
        int rArmFront; //front and back angle
        int rArmSide; //side angle
        int rArmRot; //Rotation of the arm

        //Elbow angle
        int rElbowAng;
        int lElbowAng;

        //Right leg joint
        int rLegFront;
        int rLegSide;
        int rLegRot;

        //Left leg joint
        int lLegFront;
        int lLegSide;
        int lLegRot;

        //Knee angle
        int rKneeAng;
        int lKneeAng;

        //torso angle
        int torsoBend;
        int torsoTurn;
        int torsoTilt;

        //Rest to be added soon
        //Pendulum clock angle
        int pendAng;
        bool pendMotion;

        //Angles for the fan
        int fanAng;

        //Lights
        bool light;
        bool lamp;

        //Methods 
        //Constructor
        State();
        
        ////Boolean identifies two possible motions
        bool openBox(bool type);

        void bendHead(bool type); 
        void tiltHead(bool type);
        void turnHead(bool type);

        //0 for right and 1 for left
        void sideArm(bool arm, bool type);
        void frontArm(bool arm, bool type);
        void rotateArm(bool arm, bool type);

        void bendElbow(bool arm, bool type);

        void bendTorso(bool type);
        void turnTorso(bool type);
        void tiltTorso(bool type);
        
        //0 for right and 1 for left
        void sideLeg(bool leg, bool type);
        void frontLeg(bool leg, bool type);
        void rotateLeg(bool leg, bool type);

        void bendKnee(bool leg, bool type);

        void updateTime();
        void rotateFan();
        void openDoor(bool type);

        //Prints all the state variables
        void printStates();

        //Saving poses to file
        void savePoseList(std::string fileName);

        //Loading poses from file
        void loadPoseList(std::string fileName);
        
        //Save current pose into POSE
        Pose saveStateToPose();

        //Load from POSE
        void loadStateFromPose(Pose pose);

        //Generate intermediate states and update the current state
        void animateState();

        //Clear pose list and just add current state as a keyframe
        void clearState();

        //Add the current sate as a keyframe
        void addState();
};

#endif
