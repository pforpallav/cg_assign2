#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <cstring>
#include "SOIL/src/SOIL.h"


#include "Path.h"
#include "drawFunctions.h"
#include "state.h"
#include "pose.h"

#define noTextures 14

//Controlling Mode
// 0 - Point Select
// 1 - Preview Spline/Animation
// 2 - Animate
// ` - Select keyframes
int mode = 2;
GLfloat pointPlane = 10;
bool cameraDone = false;
bool boxDone = false;
bool timerOn = true;


//State variable
State* myState = new State();
Path* myPath = new Path();
int winWidth = 500;
int winHeight = 500;
int FPS = 25;
int camPos = 0;
float stepSize = 0.003;

//Light positions
GLfloat light_position1[] = { 5.0, 8.2, 5.0, 1.0 };
GLfloat light_ambient1[] = { 0.3, 0.3, 0.3, 1.0 };
GLfloat light_diffuse1[] = { 0.6, 0.6, 0.4, 1.0 };
GLfloat light_specular1[] = { 0.6, 0.6, 0.4, 1.0 };
GLfloat spot_direction1[] = { 0.0, -1.0, -0.5 };

GLfloat light_position2[] = { 0.0, 14.8, 0.0, 1.0 };
GLfloat light_ambient2[] = { 0.4, 0.4, 0.4, 1.0 };
GLfloat light_diffuse2[] = { 0.4, 0.4, 0.4, 1.0 };
GLfloat light_specular2[] = { 0.4, 0.4, 0.4, 1.0 };
GLfloat spot_direction2[] = { -1.0, -1.0, 0.0 };

GLfloat lmodel_ambient[] = { 0.1, 0.1, 0.1, 1.0 };

//Variables for viewing
double rotate_y = 0;
double rotate_x = 0;
int viewR = 0;

//Methods
void specialKeys(int, int, int);
void keys(unsigned char, int, int);
int loadTexture();
void mouse(int, int, int, int);
void timer(int arg);
int WindowDump(void);

//Globals for texture
GLuint texture[noTextures];
GLUquadric* qobj;
GLUquadric* qobj2;
std::vector<std::string> textureFiles;

////////////////////////////////////////////////////////////////////////
// Lower case and upper case of a letter affect the same DOF in opposite sense
// Q, W, E - Left Arm three DOF 
// R - Left elbow
//
// P, O , I - Right Arm three DOF
// U - Right elbow
//
// F , G , H - Head and neck DOFs
//
// Z, X, C - Left Leg three DOF 
// V - Left knee
//
// M, < , > - Right Leg three DOF
// N - Right knee
//
// T , Y , ^ - Torso DOFs
//
// B - Opening and closing box
// 
// 7/&- Opening and closing the door
//
// Modes
// ---------
// 1 - Point Selection Mode
//        \ - Undo the last point drawn
//        | - Clear all the points
// 2 - Bezier Curve Preview Mode
// 3 - Animate Camera Mode
// 4 - Keyframe Chooser
//       8 - Add Frame
//       9 - Clear all frames and start with the current frame
//       0 - Save keyframes to file 
// 
// +/= - Turn ON/OFF the lamp
//
// -/_ - Turn ON/OFF the ceiling light
//
///////////////////////////////////////////////////////////////////////
 
void init(void) {
    //load textures
    if(!loadTexture())
      std::cout << "Texture not loaded!\n";
    
    //init the quadrics
    qobj = gluNewQuadric();
    gluQuadricNormals(qobj, GLU_SMOOTH);
    gluQuadricDrawStyle(qobj, GLU_LINE);

    qobj2 = gluNewQuadric();
    gluQuadricNormals(qobj2, GLU_SMOOTH);
    gluQuadricTexture(qobj2, 1);

    glClearColor (0.0, 0.0, 0.0, 0.0);

    initializeLists(myState);
    //Initializing light sources
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 0.0 };
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel (GL_SMOOTH);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

    //Light0 - spotlight
    glLightfv(GL_LIGHT0, GL_POSITION, light_position1);
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient1);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse1);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular1);
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.15);
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.01);
    //glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45.0);
    //glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction1);
    //light1 - overall light
    glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient2);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse2);
    glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular2);
    glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
    glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1);
    glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0);
    glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0);
    //ambient light
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

    //glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    //glEnable(GL_COLOR_MATERIAL);
    
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);


    //Creating points to check path generation
    myPath->addPoint(Point(10, 7, 10));
    myPath->addPoint(Point(-20, 7, 20));
    myPath->addPoint(Point(-10, 7, -10));
    myPath->addPoint(Point(0, 20, 0));
    myPath->addPoint(Point(-20, 20, -10));
    myPath->addPoint(Point(-10, 15, -30));
    myPath->addPoint(Point(0, 7, 60));
    myPath->addPoint(Point(0, 7, 70));
    myPath->addPoint(Point(0, 7, 80));
    myPath->addPoint(Point(0, 7, 90));
    myPath->addPoint(Point(0, 7, 100));

    myPath->getSamplePoints(stepSize);

    myState->loadPoseList(std::string("Pose.txt"));
    //myState->moveList.push_back(myState->saveStateToPose());
    //myState->savePoseList(std::string("newPose.txt"));
}

void display(void){
   glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glColor3f (1.0, 1.0, 1.0);
   glLoadIdentity ();
    /* clear the matrix */
    
   if(true/*mode != 0*/){
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 200.0);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
   }else{
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    //glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble near, GLdouble far);
    glOrtho(-50.0, 50.0, 0.0, 20.0, 1.5, 200);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
   }
   
   /* viewing transformation  */
   //gluLookAt (0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
   if(mode == 2){
    gluLookAt (myPath->lastSamplePts[camPos].x, myPath->lastSamplePts[camPos].y, myPath->lastSamplePts[camPos].z, 0.0, 7.0, 0.0, 0.0, 1.0, 0.0);
   }
   else if(mode == 1  || mode == 3)
    //gluLookAt (0.0, 7.0, 70.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
    //Look at changed for animation testing for closer look up
    gluLookAt (0.0, 7.0, 7.0, 0.0, 7.0, 0.0, 0.0, 1.0, 0.0);
   else
    gluLookAt (0.0, 10.0, 10.0, 0.0, 10.0, 0.0, 0.0, 1.0, 0.0);

   if(mode == 1 || mode == 3)
    glTranslatef(0.0, 0.0, viewR);
   else if(mode == 0)
    glTranslatef(0.0, 0.0, -40);

   if(mode == 1 || mode == 3)
    glRotatef( rotate_y, 0.0, 1.0, 0.0 );
   
   glLightfv(GL_LIGHT1, GL_POSITION, light_position2);
   glLightfv(GL_LIGHT0, GL_POSITION, light_position1);
   //glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45.0);
   //glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spot_direction1);
   glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
   //myPath->drawControlPoints();
   //myPath->getSamplePoints(0.05);
   drawScene(myState);
   if(mode == 0){
    drawPlane(pointPlane);
    myPath->drawControlPoints();
   } else if(mode == 1){
    myPath->drawPoints(myPath->lastSamplePts);
    //Changed for animation debugging (Uncomment above line and comment below lines for re-storing back to stable A22 submission)
    //myState->boxOpenAng = 120;
   }

   glutSwapBuffers();
   glFlush ();
}

void reshape (int w, int h){
   glEnable(GL_DEPTH_TEST);
   glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
   if(mode!=0){
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    glFrustum (-1.0, 1.0, -1.0, 1.0, 1.5, 200.0);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
   }else{
    glMatrixMode (GL_PROJECTION);
    glLoadIdentity ();
    //glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble near, GLdouble far);
    glOrtho(-50.0, 50.0, -10.0, 30.0, 1.5, 200);
    glMatrixMode (GL_MODELVIEW);
    glLoadIdentity ();
   }
}
// ----------------------------------------------------------
// main() function
// ----------------------------------------------------------
int main(int argc, char** argv){
   glutInit(&argc, argv);
   glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
   glutInitWindowSize (winWidth, winHeight);
   glutInitWindowPosition (100, 100);
   glutCreateWindow (argv[0]);
   init ();
   glutSpecialFunc(specialKeys);
   glutDisplayFunc(display);
   glutKeyboardFunc(keys);
   glutMouseFunc(mouse);
   glutReshapeFunc(reshape);
   glutTimerFunc(100, timer, 0);
   glutMainLoop();
   return 0;
}
// ----------------------------------------------------------
// specialKeys() Callback Function
// ----------------------------------------------------------
void specialKeys( int key, int x, int y ) {
  //  Right arrow - increase rotation by 5 degree
  if(mode == 2)
    return;

  if (key == GLUT_KEY_RIGHT){
    if(mode == 1  || mode == 3)
      rotate_y += 5;
  }
 
  //  Left arrow - decrease rotation by 5 degree
  else if (key == GLUT_KEY_LEFT){
    if(mode == 1  || mode == 3)
      rotate_y -= 5;
  }
 
  else if (key == GLUT_KEY_UP){
    if(mode == 1 || mode == 3){
      viewR += 2;
      rotate_x += 5;
    } else {
      //std::cout << pointPlane << std::endl;
      if(pointPlane < 35)
        pointPlane++;
    }
  }
 
  else if (key == GLUT_KEY_DOWN){
    if(mode == 1 || mode == 3){
      viewR -= 2; 
      rotate_x -= 5;
    }else{
      //std::cout << pointPlane << std::endl;
      if(pointPlane > -35)
        pointPlane--;
    }
  }
  //  Request display update
  glutPostRedisplay();
}

void keys(unsigned char c, int x, int y){
  //Switching based on the input
  std::string f;
          char* a;
  switch(c){
    case '4':
        mode = 3;
        break;

    case '8':
        if(mode != 3) return;
        myState->addState();
        break;

    case '9':
        if(mode != 3) return;
        myState->clearState();
        break;

    case '0':
        if(mode != 3) return;
        printf("Please enter the filename to save keyframes: ");
        scanf("%s", a);
        f = std::string(a);
        myState->savePoseList(f);
        break;
        
    case '|':
        if(mode != 0) return;
        myPath->clear();
        myPath->getSamplePoints(stepSize);
        break;

    case '\\':
        if(mode != 0) return;
        myPath->undo();
        myPath->getSamplePoints(stepSize);
        break;

    case '=':
        myState->lamp = false;
        glDisable(GL_LIGHT0);
        break;

    case '+':
        myState->lamp = true;
        glEnable(GL_LIGHT0);
        break;

    case '-':
        myState->light = false;
        glDisable(GL_LIGHT1);
        break;

    case '_':
        myState->light = true;
        glEnable(GL_LIGHT1);
        break;

    case '1':
        mode = 0;
        break;

    case '2':
        mode = 1;
        if(!timerOn){
          timerOn = true;
          glutTimerFunc(1000/FPS, timer, 0);
        }
        break;

    case '3':
        mode = 2;
        printf("Please enter the filename to load keyframes: ");
        scanf("%s", a);
        f = std::string(a);
        myState->loadPoseList(std::string(f));
        if(!timerOn){
          timerOn = true;
          glutTimerFunc(1000/FPS, timer, 0);
        }
        break;

    case 'b':
        myState->openBox(true);
        break;

    case 'B':
        myState->openBox(false);
        break;

    case '7':
        myState->openDoor(true);
        break;

    case '&':
        myState->openDoor(false);
        break;

    case 'f':
        myState->bendHead(true);
        break;

    case 'F':
        myState->bendHead(false);
        break;
  
    case 'g':
        myState->turnHead(true);
        break;

    case 'G':
        myState->turnHead(false);
        break;
  
    case 'h':
        myState->tiltHead(true);
        break;

    case 'H':
        myState->tiltHead(false);
        break;

    case 'q':
        myState->frontArm(false, true);
        break;

    case 'Q':
        myState->frontArm(false, false);
        break;
    
    case 'w':
        myState->sideArm(false, true);
        break;

    case 'W':
        myState->sideArm(false, false);
        break;

    case 'e':
        myState->rotateArm(false, true);
        break;

    case 'E':
        myState->rotateArm(false, false);
        break;

    case 'i':
        myState->frontArm(true, true);
        break;

    case 'I':
        myState->frontArm(true, false);
        break;
    
    case 'o':
        myState->sideArm(true, true);
        break;

    case 'O':
        myState->sideArm(true, false);
        break;

    case 'p':
        myState->rotateArm(true, true);
        break;

    case 'P':
        myState->rotateArm(true, false);
        break;
    
    case 'r':
        myState->bendElbow(false, true);
        break;

    case 'R':
        myState->bendElbow(false, false);
        break;

    case 'u':
        myState->bendElbow(true, true);
        break;

    case 'U':
        myState->bendElbow(true, false);
        break;

    case 't':
        myState->bendTorso(true);
        break;

    case 'T':
        myState->bendTorso(false);
        break;

    case 'y':
        myState->turnTorso(true);
        break;

    case 'Y':
        myState->turnTorso(false);
        break;

    case '6':
        myState->tiltTorso(true);
        break;

    case '^':
        myState->tiltTorso(false);
        break;

    case 'z':
        myState->sideLeg(false, true);
        break;

    case 'Z':
        myState->sideLeg(false, false);
        break;

    case 'x':
        myState->frontLeg(false, true);
        break;

    case 'X':
        myState->frontLeg(false, false);
        break;

    case 'c':
        myState->rotateLeg(false, true);
        break;

    case 'C':
        myState->rotateLeg(false, false);
        break;

    case 'v':
        myState->bendKnee(false, true);
        break;

    case 'V':
        myState->bendKnee(false, false);
        break;

    case 'n':
        myState->sideLeg(true, true);
        break;

    case 'N':
        myState->sideLeg(true, false);
        break;

    case 'm':
        myState->frontLeg(true, true);
        break;

    case 'M':
        myState->frontLeg(true, false);
        break;

    case ',':
        myState->rotateLeg(true, true);
        break;

    case '<':
        myState->rotateLeg(true, false);
        break;

    case '.':
        myState->bendKnee(true, true);
        break;

    case '>':
        myState->bendKnee(true, false);
        break;

    case 27:
        exit(0);
        break;

    default:
        break;
  }
  //Forcing re-displaying
  glutPostRedisplay();
}

void mouse(int button, int state, int x, int y) {
  if(mode == 0){
    if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        
        //std::cout << x << " " << y << " " << pointPlane <<  std::endl;
        float X = (float)x/(winWidth/(20+80*(35-pointPlane)/61)) - (20+80*(35-pointPlane)/61)/2;
        float Y;
        float YUpper = 500 - ((float)(500-305)/61)*(35 - pointPlane);
        float YLower = (float)201*(35 - pointPlane)/61;
        if(X < -50)
          X = -50;
        if(X > 50)
          X = 50;
        if(y > YUpper)
          Y = 0;
        else if(y < YLower)
          Y = 20;
        else
          Y = 20 - ((y - YLower)/(YUpper - YLower))*20;
        //std::cout << X << " " << Y << " " << pointPlane <<  std::endl;
        myPath->addPoint(Point(X, Y, pointPlane));
        myPath->getSamplePoints(stepSize);
        glutPostRedisplay();
        //int depth = 70-viewR;
        //myPath->addPoint(Point(xImg/(70-viewR), yImg/(70-viewR), 70-viewR));
        //printf("Clicked at (x,y) : %d, %d \n", x, y);
        //printf("Clicked at (X,Y,Z) : %f, %f %d\n",(float)100*xImg/depth, (float)100*yImg/depth, depth);
    }
  }
}
// ----------------------------------------------------------
// Load the texture
// ----------------------------------------------------------
void timer(int arg){
    if(mode == 2){ 
        WindowDump();
        myState->openDoor(true);
        if(camPos+1 >= 1/stepSize)
            cameraDone = true;
        else
            camPos++;

        if(cameraDone && !boxDone){
            boxDone = myState->openBox(true);
        }
    }else{
        camPos = 0;
        myState->doorOpen = 0;
        cameraDone = false;
        boxDone = false;
    }

    myState->updateTime();
    myState->rotateFan();
    glutPostRedisplay();
    if(mode == 2){
      glutTimerFunc(1000/FPS, timer, arg);
      if(cameraDone && boxDone)
        myState->animateState();
    }
    //Added for animation debugging(uncomment below lines for restoring)
    else if(mode == 1){
      glutTimerFunc(1000/FPS, timer, arg);
      myState->animateState();
    }else {
      timerOn = false;
    }
}
// ----------------------------------------------------------
// Load the texture
// ----------------------------------------------------------
int loadTexture(){
    //Including the file names for the textures
    textureFiles.push_back(std::string("wood-texture1.bmp"));
    textureFiles.push_back(std::string("wood-texture2.bmp"));
    textureFiles.push_back(std::string("box-texture1.bmp"));
    textureFiles.push_back(std::string("box-texture2.bmp"));
    textureFiles.push_back(std::string("ivory-texture.bmp"));
    textureFiles.push_back(std::string("hat-texture.bmp"));
    textureFiles.push_back(std::string("wall-texture.bmp"));
    textureFiles.push_back(std::string("floor-texture.bmp"));
    textureFiles.push_back(std::string("ceiling-texture.bmp"));
    textureFiles.push_back(std::string("bookshelf-texture.bmp"));
    textureFiles.push_back(std::string("shoeshelf-texture.bmp"));
    textureFiles.push_back(std::string("monalisa-texture.bmp"));
    textureFiles.push_back(std::string("wallclock-texture.bmp"));
    textureFiles.push_back(std::string("door-texture.bmp"));

    //Loading the textures
    for (int i = 0; i < textureFiles.size(); i++){
      texture[i] =  SOIL_load_OGL_texture(textureFiles[i].c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_INVERT_Y);
    }

    //Checking for valid loading
    for (int i = 0; i < textureFiles.size(); i++){
        if(texture[i] == 0){
           printf("Texture at %s not loaded correctly! \n", textureFiles[i].c_str());
           return false;
        }
    }

  // Typical Texture Generation Using Data From The Bitmap
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
  glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
  glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);

  return true;
}

int WindowDump(void){
   int i,j;
   FILE *fptr;
   static int counter = 0; /* This supports animation sequences */
   char fname[32];
   unsigned char *image;
   int width = winWidth;
   int height = winHeight;
   bool stereo = false;

   /* Allocate our buffer for the image */
   if ((image = (unsigned char*)malloc(3*width*height*sizeof(char))) == NULL) {
      fprintf(stderr,"Failed to allocate memory for image\n");
      return(false);
   }

   glPixelStorei(GL_PACK_ALIGNMENT,1);

   /* Open the file */
   if (stereo)
      sprintf(fname,"L_%04d.raw",counter);
   else
      sprintf(fname,"../images/C_%04d.raw",counter);
   if ((fptr = fopen(fname,"w")) == NULL) {
      fprintf(stderr,"Failed to open file for window dump\n");
      return(false);
   }

   /* Copy the image into our buffer */
   glReadBuffer(GL_BACK_LEFT);
   glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);

   /* Write the raw file */
   /* fprintf(fptr,"P6\n%d %d\n255\n",width,height); for ppm */
   for (j=height-1;j>=0;j--) {
      for (i=0;i<width;i++) {
         fputc(image[3*j*width+3*i+0],fptr);
         fputc(image[3*j*width+3*i+1],fptr);
         fputc(image[3*j*width+3*i+2],fptr);
      }
   }
   fclose(fptr);

   if (stereo) {
      /* Open the file */
      sprintf(fname,"R_%04d.raw",counter);
      if ((fptr = fopen(fname,"w")) == NULL) {
         fprintf(stderr,"Failed to open file for window dump\n");
         return(false);
      }

      /* Copy the image into our buffer */
      glReadBuffer(GL_BACK_RIGHT);
      glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,image);

      /* Write the raw file */
      /* fprintf(fptr,"P6\n%d %d\n255\n",width,height); for ppm */
      for (j=height-1;j>=0;j--) {
         for (i=0;i<width;i++) {
            fputc(image[3*j*width+3*i+0],fptr);
            fputc(image[3*j*width+3*i+1],fptr);
            fputc(image[3*j*width+3*i+2],fptr);
         }
      }
      fclose(fptr);
   }

   /* Clean up */
   counter++;
   free(image);
   return(true);
}
