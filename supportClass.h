#ifndef SUPPORT_CLASS
#define SUPPORT_CLASS
//File has list of all supporting classes used for better management

class Color{
    public:
        float R, G, B;

    //Constructors
    Color();
    Color(float r, float g, float b);
};

#endif
//////////////////////////////////////
