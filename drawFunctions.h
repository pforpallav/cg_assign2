#ifndef DRAW_FUNCS
#define DRAW_FUNCS

#include <iostream>
#include <stdio.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "Path.h"
#include "supportClass.h"
#include "state.h"

#define GRID 0
#define noTextures 14

extern GLuint texture[noTextures];
extern GLUquadric* qobj;
extern GLUquadric* qobj2;

//File contains a list of functions that draw the basic rigid objects
void drawBox(State* state);

void drawOpenCuboid(Color* boxColor, Color* edgeColor, bool tex, int textureNum);

void drawBorder(Color* borderColor);

void drawBoxPart(State* state);

void drawBoxPart2(State* state);

void drawPerson(State* state);

void drawHead(State* state);

void drawLimb(State* state);

void drawJoint(State* state);

void drawTorso(State* state);

void drawHand(State* state, bool arm);

void drawNeck(State* state);

void drawLeg(State* state, bool leg);

void drawHat(State* state);

void drawStick(State* state);

//Drawing functions to render scene
void drawWall(State* state);

void drawFloor(State* state);

void drawCeiling(State* state);

void drawTable(State* state);

void drawTableLeg(State* state);

void drawChair(State* state);

void drawChairLeg(State* state);

void drawChairBack(State* state);

void drawBookShelf(State* state);

void drawShoeShelf(State* state);

void drawPotrait(State* state, int textureInd);

void drawClock(State* state);

void drawFan(State* state);

void drawPlane(GLfloat z);

void drawWallLight(State* state);

void drawLamp(State* state);

void drawScene(State* state);
//Method to initialize display lists
void initializeLists(State* state);
#endif
