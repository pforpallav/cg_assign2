#include "drawFunctions.h"
#include "colors.h"
///////////////////////////////////////////////////
//Function to draw the box with openAng between lid and box
void drawBox(State* state){
    int openAng = state->boxOpenAng;
    //Rotating the lid
    glPushMatrix();
        glTranslatef(0.0, 0.5, -1.0);
        glRotatef(360-openAng, 1.0, 0.0, 0.0);
        glTranslatef(0.0, 1.0, 1.0);
        glCallList(state->boxPartList);
    glPopMatrix();
    //Drawing the base
    glPushMatrix();
        glScalef(1.0, 0.5, 1.0);
        glCallList(state->boxPartList2);
    glPopMatrix();
}

//Function to draw box part and encapsulate in display list
void drawBoxPart(State* state){
    //Creating display list
    state->boxPartList = glGenLists(1);
    glNewList(state->boxPartList, GL_COMPILE);
        //Lid
        glPushMatrix();
            glScalef(1.0, -1.0, 1.0);
            glPushMatrix();
                glTranslatef(0.0, 0.1, 0.0);
                glScalef(0.9, 1, 0.9);
                drawOpenCuboid(black, black, 0, 0);
            glPopMatrix();
            glPushMatrix();
                glTranslatef(0.0, 0.2, 0.0);
                glScalef(0.8, 0.9, 0.8);
                drawOpenCuboid(black, black, 0, 0);
            glPopMatrix();

            //Outer wall
            drawOpenCuboid(black, white, 1, 2);
            //Border1
            glColor3f(dred->R, dred->G, dred->B);
            glBegin(GL_QUADS);
                glNormal3f(0.0, 1.0, 0.0);

                glVertex3f(1.0, 1.0, 1.0);
                glVertex3f(1.0, 1.0, -1.0);
                glVertex3f(0.9, 1.0, -1.0);
                glVertex3f(0.9, 1.0, 1.0);
                
                glVertex3f(-1.0, 1.0, -1.0);
                glVertex3f(-1.0, 1.0, 1.0);
                glVertex3f(-0.9, 1.0, 1.0);
                glVertex3f(-0.9, 1.0, -1.0);

                glVertex3f(-0.9, 1.0, 0.9);
                glVertex3f(-0.9, 1.0, 1.0);
                glVertex3f(0.9, 1.0, 1.0);
                glVertex3f(0.9, 1.0, 0.9);

                glVertex3f(0.9, 1.0, -0.9);
                glVertex3f(0.9, 1.0, -1.0);
                glVertex3f(-0.9, 1.0, -1.0);
                glVertex3f(-0.9, 1.0, -0.9);
            glEnd();
            //Border2
            glBegin(GL_QUADS);
                glNormal3f(0.0, 1.0, 0.0);

                glVertex3f(0.9, 1.1, 0.9);
                glVertex3f(0.9, 1.1, -0.9);
                glVertex3f(0.8, 1.1, -0.9);
                glVertex3f(0.8, 1.1, 0.9);
                
                glVertex3f(-0.9, 1.1, -0.9);
                glVertex3f(-0.9, 1.1, 0.9);
                glVertex3f(-0.8, 1.1, 0.9);
                glVertex3f(-0.8, 1.1, -0.9);

                glVertex3f(-0.8, 1.1, 0.8);
                glVertex3f(-0.8, 1.1, 0.9);
                glVertex3f(0.8, 1.1, 0.9);
                glVertex3f(0.8, 1.1, 0.8);

                glVertex3f(0.8, 1.1, -0.8);
                glVertex3f(0.8, 1.1, -0.9);
                glVertex3f(-0.8, 1.1, -0.9);
                glVertex3f(-0.8, 1.1, -0.8);
            glEnd();
            //drawBorder(red);
        glPopMatrix();
    glEndList();
}

void drawBoxPart2(State* state){
    //Creating display list
    state->boxPartList2 = glGenLists(1);
    glNewList(state->boxPartList2, GL_COMPILE);
        //Base
        glPushMatrix();
            glPushMatrix();
                glTranslatef(0.0, 0.1, 0.0);
                glScalef(0.9, 0.9, 0.9);
                drawOpenCuboid(black, white, 0, 0);
            glPopMatrix();
            glPushMatrix();
                glTranslatef(0.0, 0.1, 0.0);
                glScalef(0.8, 0.7, 0.8);
                drawOpenCuboid(black, white, 0, 0);
            glPopMatrix();

            //Outer wall
            drawOpenCuboid(black, white, 1, 3);
            //Border1
            glColor3f(dred->R, dred->G, dred->B);
            glBegin(GL_QUADS);
                glNormal3f(0.0, 1.0, 0.0);
                glVertex3f(1.0, 1.0, 1.0);
                glVertex3f(1.0, 1.0, -1.0);
                glVertex3f(0.9, 1.0, -1.0);
                glVertex3f(0.9, 1.0, 1.0);
                
                glVertex3f(-1.0, 1.0, -1.0);
                glVertex3f(-1.0, 1.0, 1.0);
                glVertex3f(-0.9, 1.0, 1.0);
                glVertex3f(-0.9, 1.0, -1.0);

                glVertex3f(-0.9, 1.0, 0.9);
                glVertex3f(-0.9, 1.0, 1.0);
                glVertex3f(0.9, 1.0, 1.0);
                glVertex3f(0.9, 1.0, 0.9);

                glVertex3f(0.9, 1.0, -0.9);
                glVertex3f(0.9, 1.0, -1.0);
                glVertex3f(-0.9, 1.0, -1.0);
                glVertex3f(-0.9, 1.0, -0.9);
            glEnd();
            //Border2
            glPushMatrix();
                //glTranslatef(0.0, -0.5, 0.0);
                glBegin(GL_QUADS);
                    glVertex3f(0.9, 0.8, 0.9);
                    glVertex3f(0.9, 0.8, -0.9);
                    glVertex3f(0.8, 0.8, -0.9);
                    glVertex3f(0.8, 0.8, 0.9);
                    
                    glVertex3f(-0.9, 0.8, -0.9);
                    glVertex3f(-0.9, 0.8, 0.9);
                    glVertex3f(-0.8, 0.8, 0.9);
                    glVertex3f(-0.8, 0.8, -0.9);

                    glVertex3f(-0.8, 0.8, 0.8);
                    glVertex3f(-0.8, 0.8, 0.9);
                    glVertex3f(0.8, 0.8, 0.9);
                    glVertex3f(0.8, 0.8, 0.8);

                    glVertex3f(0.8, 0.8, -0.8);
                    glVertex3f(0.8, 0.8, -0.9);
                    glVertex3f(-0.8, 0.8, -0.9);
                    glVertex3f(-0.8, 0.8, -0.8);
                glEnd();
            glPopMatrix();
            //drawBorder(green);
        glPopMatrix();
    glEndList();
}

//Function to draw an open cuboid
void drawOpenCuboid(Color* boxColor, Color* edgeColor, bool tex, int textureNum){
    //Pushing and ensuring popped at the end
    glPushMatrix();
    
    if(tex){
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[textureNum]);
    } else {
        GLfloat blackColor[] = {0, 0, 0, 1.0};
        glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, blackColor);
    }

    //Inner box 
    glColor3f(boxColor->R, boxColor->G, boxColor->B);
    glBegin(GL_QUADS);
        //Back face
        glNormal3f(0,0,-1);
        glTexCoord2f(1.0, 0);
        glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2f(1,1);
        glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2f(0,1);
        glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(0,0);
        glVertex3f(-1.0, 1.0, -1.0);

        //Front face
        glNormal3f(0,0,1);
        glTexCoord2f(1,0);
        glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2f(1,1);
        glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2f(0,1);
        glVertex3f(-1.0, -1.0, 1.0);
        glTexCoord2f(0,0);
        glVertex3f(-1.0, 1.0, 1.0);

        //Side face
        glNormal3f(1,0,0);
        glTexCoord2f(0,0);
        glVertex3f(1.0, 1.0, 1.0);
        glTexCoord2f(1,0);
        glVertex3f(1.0, 1.0, -1.0);
        glTexCoord2f(1,1);
        glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2f(0,1);
        glVertex3f(1.0, -1.0, 1.0);

        //Side face
        glNormal3f(-1,0,0);
        glTexCoord2f(0,0);
        glVertex3f(-1.0, 1.0, 1.0);
        glTexCoord2f(1,0);
        glVertex3f(-1.0, 1.0, -1.0);
        glTexCoord2f(1,1);
        glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(0,1);
        glVertex3f(-1.0, -1.0, 1.0);

        //Bottom face
        glNormal3f(0,-1,0);
        glTexCoord2f(1,0);
        glVertex3f(1.0, -1.0, 1.0);
        glTexCoord2f(1,1);
        glVertex3f(1.0, -1.0, -1.0);
        glTexCoord2f(0,1);
        glVertex3f(-1.0, -1.0, -1.0);
        glTexCoord2f(0,0);
        glVertex3f(-1.0, -1.0, 1.0);
    glEnd();

    #if GRID
        glColor3f(edgeColor->R, edgeColor->G, edgeColor->B);
        glutWireCube(2.0);
    #endif

    glDisable(GL_TEXTURE_2D);
    GLfloat ambientDef[] = {0.2, 0.2, 0.2, 1.0};
    glMaterialfv(GL_FRONT, GL_AMBIENT, ambientDef);
    GLfloat diffuseDef[] = {0.8, 0.8, 0.8, 1.0};
    glMaterialfv(GL_FRONT, GL_AMBIENT, diffuseDef);
    //Re-setting previous value
    glPopMatrix();
}

void drawBorder(Color* borderColor){
    //Strip inbetween
    glColor3f(borderColor->R, borderColor->G, borderColor->B);
    glBegin(GL_QUADS);
        glNormal3f(0.0, 1.0, 0.0);
        glVertex3f(1.0, 1.0, 1.0);
        glVertex3f(1.0, 1.0, -1.0);
        glVertex3f(0.8, 1.0, -1.0);
        glVertex3f(0.8, 1.0, 1.0);
        
        glNormal3f(0.0, 1.0, 0.0);
        glVertex3f(-1.0, 1.0, -1.0);
        glVertex3f(-1.0, 1.0, 1.0);
        glVertex3f(-0.8, 1.0, 1.0);
        glVertex3f(-0.8, 1.0, -1.0);

        glNormal3f(0.0, 1.0, 0.0);
        glVertex3f(-0.8, 1.0, 0.8);
        glVertex3f(-0.8, 1.0, 1.0);
        glVertex3f(0.8, 1.0, 1.0);
        glVertex3f(0.8, 1.0, 0.8);

        glNormal3f(0.0, 1.0, 0.0);
        glVertex3f(0.8, 1.0, -0.8);
        glVertex3f(0.8, 1.0, -1.0);
        glVertex3f(-0.8, 1.0, -1.0);
        glVertex3f(-0.8, 1.0, -0.8);
    glEnd();
}
/////////////////////////////////////////////////////
//Function to draw the head of the figure
void drawPerson(State* state){
    initializeLists(state);
    glTranslatef(0.0, 4.0, 0.0);
    glPushMatrix();
        //glTranslate();
        glPushMatrix();
            //glTranslatef(0.0, 8.0, 0.0);
            glRotatef(state->torsoBend, 1.0, 0.0, 0.0);
            glRotatef(state->torsoTurn, 0.0, 1.0, 0.0);
            glRotatef(state->torsoTilt, 0.0, 0.0, 1.0);
            glTranslatef(0.0, 6.0, 0.0);
            //Head and Neck
            glPushMatrix();
                glTranslatef(0.0, -0.8, 0.0);
                glCallList(state->headList); 
                //glPushMatrix();
                //    glTranslatef(0.0, 2.0, 0.0);
                //    glScalef(0.5, -0.5, 0.5);
                //    glCallList(state->hatList);
                //glPopMatrix();
                glScalef(0.3, 0.1, 0.3);
                glRotatef(90, 1.0, 0.0, 0.0);
                glCallList(state->neckList);
            glPopMatrix();
            //Upper and lower torso
            glTranslatef(0.0, -6.0, 0.0);
            glCallList(state->torsoList);

            //Both the hands 
            glPushMatrix();
                glTranslatef(-2, 4, 0.0);
                glRotatef(-40, 0.0, 0.0, 1.0);
                drawHand(state, true);
            glPopMatrix();
            glPushMatrix();
                glTranslatef(2, 4, 0.0);
                glRotatef(40, 0.0, 0.0, 1.0);
                drawHand(state, false);
            glPopMatrix();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, -1.6, 0);
            glScalef(0.75, 0.3, 0.75);
            glCallList(state->torsoList);
        glPopMatrix();
        
        //Both the legs
        glPushMatrix();
            glScalef(0.5, 0.5, 0.5);
            glTranslatef(0.0, -9.5, 0.0);
            glPushMatrix();
                glTranslatef(-2, 4, 0.0);
                drawLeg(state, true);
            glPopMatrix();
            glPushMatrix();
                glTranslatef(2, 4, 0.0);
                drawLeg(state, false);
            glPopMatrix();

            //Tranlating for whole body
            glTranslatef(1.5, -1.5, 0.0);
        glPopMatrix();
    glPopMatrix();
}

void drawHead(State* state){
    state->headList = glGenLists(1);
    glNewList(state->headList, GL_COMPILE);
        //glColor3f(brown->R, brown->G, brown->B);

        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glPushMatrix();
            glTranslatef(0.0, -0.168, 0.0);
            glRotatef(state->headBend, 1.0, 0.0, 0.0);
            glRotatef(state->headTilt, 0.0, 0.0, 1.0);
            glRotatef(state->headTurn, 0.0, 1.0, 0.0);
            glTranslatef(0.0, 0.168, 0.0);
            glScalef(0.45, 0.35, 0.45);
            glPushMatrix();
                glPushMatrix();
                  glScalef(1, 0.8, 1);
                  //glColor3f (0.5, 0.2, 0.2);
                  gluSphere(qobj2, 1.2, 20, 20);
                  #if GRID
                    glColor3f(1,1,1);
                    gluSphere(qobj, 1.2, 20, 20);
                  #endif
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, texture[1]);
                glPushMatrix();
                  glRotatef(270, 1, 0, 0);
                  #if GRID
                    glColor3f(1,1,1);
                    gluCylinder(qobj, 1.2, 2.0, 4, 40, 16);
                  #endif
                  //glColor3f (0.5, 0.2, 0.2);
                  gluCylinder(qobj2, 1.2, 2.0, 4, 40, 16);
                glPopMatrix();
                glBindTexture(GL_TEXTURE_2D, texture[0]);
                glPushMatrix();
                  glTranslatef(0, 4, 0);
                  //glScalef(1, 0.8, 1);
                  //glColor3f (0.5, 0.2, 0.2);
                  gluSphere(qobj2, 2, 20, 20);
                  #if GRID
                    glColor3f(1,1,1);
                    gluSphere(qobj, 2, 20, 20);
                  #endif
                glPopMatrix();
                glPushMatrix();
                    glTranslatef(0.0, 5.0, 0.0);
                    glScalef(1, -1, 1);
                    glCallList(state->hatList);
                glPopMatrix();
            glPopMatrix();
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawJoint(State* state){
    state->jointList = glGenLists(1);
    glNewList(state->jointList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[1]);

        //glColor3f(brown->R, brown->G, brown->B);
        gluSphere(qobj2, 1, 20, 20);

        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawLimb(State* state){
    state->limbList = glGenLists(1);
    glNewList(state->limbList, GL_COMPILE);
        //glColor3f(brown->R, brown->G, brown->B);

        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glPushMatrix();
            glScalef(0.4, 1.5, 0.4);
            gluSphere(qobj2, 1, 20, 20);
            //Just to check the correctness of the model
            #if GRID
                glColor3f(black->R, black->G, black->B);
                gluSphere(qobj, 1, 20, 20);
            #endif
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawTorso(State* state){
    state->torsoList = glGenLists(1);
    glNewList(state->torsoList, GL_COMPILE);
        //glColor3f(brown->R, brown->G, brown->B);

        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glPushMatrix();
            glScalef(1, 1, 0.5);
            glPushMatrix();
              glScalef(1, 0.3, 1);
              //glColor3f (0.5, 0.2, 0.2);
              gluSphere(qobj2, 1.5, 20, 20);
              #if GRID
                glColor3f(1,1,1);
                gluSphere(qobj, 1.5, 20, 20);
              #endif
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, texture[1]);
            glPushMatrix();
              glRotatef(270, 1, 0, 0);
              #if GRID
                glColor3f(1,1,1);
                gluCylinder(qobj, 2.0, 1.5, 4, 40, 16);
              #endif
              //glColor3f (0.5, 0.2, 0.2);
              gluCylinder(qobj2, 1.5, 2.0, 4, 40, 16);
            glPopMatrix();
            glBindTexture(GL_TEXTURE_2D, texture[0]);
            glPushMatrix();
              glTranslatef(0, 4, 0);
              glScalef(1, 0.5, 1);
              //glColor3f (0.5, 0.2, 0.2);
              gluSphere(qobj2, 2, 20, 20);
              #if GRID
                glColor3f(1,1,1);
                gluSphere(qobj, 2, 20, 20);
              #endif
            glPopMatrix();
        glPopMatrix();

        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawHand(State* state, bool arm){
    glPushMatrix();
        glPushMatrix();
            glScalef(0.5, 0.5, 0.5);
            glCallList(state->jointList);
        glPopMatrix();
        //Left arm
        if(arm){
            glRotatef(-1*state->lArmFront, 1.0, 0.0, 0.0);
            glRotatef(-1*state->lArmSide, 0.0, 0.0, 1.0);
            glRotatef(-1*state->lArmRot, 0.0, 1.0, 0.0);
        }
        //Right arm
        else{
            glScalef(1.0, 1.0, -1.0);
            glRotatef(1*state->rArmFront, 1.0, 0.0, 0.0);
            glRotatef(1*state->rArmSide, 0.0, 0.0, 1.0);
            glRotatef(1*state->rArmRot, 0.0, 1.0, 0.0);
        }
        glTranslatef(0.0, -1.5, 0.0);
        glCallList(state->limbList);
        glPushMatrix();
            glTranslatef(0.0, -1.5, 0.0);
            glPushMatrix();
                glScalef(0.2, 0.2, 0.2);
                glCallList(state->jointList);
            glPopMatrix();
            glTranslatef(0.0, -1.5, 0.0);
            //Rotating to given angle
            glTranslatef(0.0, 1.5, 0.0);
            if(arm) glRotatef(-1*state->lElbowAng, 1.0, 0.0, 0.0);
            else glRotatef(state->rElbowAng, 1.0, 0.0, 0.0);
            glTranslatef(0.0, -1.5, 0.0);
            glCallList(state->limbList);
            if(false/*!arm*/){
                glPushMatrix();
                    glRotatef(90, 1.0, 0.0, 0.0);
                    glTranslatef(0.0, 1.0, 1.1);
                    glScalef(0.3, 0.3, 0.3);
                    glCallList(state->stickList);
                glPopMatrix();
            }
        glPopMatrix();
    glPopMatrix();
}

void drawNeck(State* state){
    state->neckList = glGenLists(1);
    glNewList(state->neckList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[1]);

        #if GRID
            glColor3f(1,1,1);
            gluCylinder(qobj, 1.5, 2.0, 4, 40, 16);
        #endif
        //glColor3f (0.5, 0.2, 0.2);
        gluCylinder(qobj2, 1.5, 2.0, 4, 40, 16);
    
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawLeg(State* state, bool leg){
    glPushMatrix();
        glTranslatef(0.0, 3.0, 0.0);
        //Rotating the hip 
        //Left leg
        if(leg){
            glRotatef(-1*state->lLegFront, 1.0, 0.0, 0.0);
            glRotatef(-1*state->lLegSide, 0.0, 0.0, 1.0);
            glRotatef(-1*state->lLegRot, 0.0, 1.0, 0.0);
        }
        //Right leg
        else{
            glScalef(1.0, 1.0, -1.0);
            glRotatef(1*state->rLegFront, 1.0, 0.0, 0.0);
            glRotatef(1*state->rLegSide, 0.0, 0.0, 1.0);
            glRotatef(1*state->rLegRot, 0.0, 1.0, 0.0);
        }
        //if(type) glRotatef(-1*state->lKneeAng, 1.0, 0.0, 0.0);
        //else glRotatef(state->rKneeAng, 1.0, 0.0, 0.0);
        glTranslatef(0.0, -3.0, 0.0);
        glPushMatrix();
            glTranslatef(0, 3.0, 0);
            glCallList(state->jointList);
        glPopMatrix();
        glPushMatrix();
            glScalef(1.3, 4, 1.3);
            glCallList(state->jointList);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, -3, 0);
            //glScalef(0.75, 0.75, 0.75);
            glCallList(state->jointList);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.0, -2.5, 0.0);
            if(leg) glRotatef(-1*state->lKneeAng, 1.0, 0.0, 0.0);
            else glRotatef(state->rKneeAng, 1.0, 0.0, 0.0);
            glTranslatef(0.0, 2.5, 0.0);
            glTranslatef(0, -7, 0);
            glScalef(1, 5, 1);
            glCallList(state->jointList);
        glPopMatrix();
        
    glPopMatrix();

}

void drawHat(State* state){
    state->hatList = glGenLists(1);
    glNewList(state->hatList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[5]);

        glPushMatrix();
            glRotatef(90, 1.0, 0.0, 0.0);
            gluCylinder(qobj2, 2.0, 2.0, 6, 40, 16);
            glPushMatrix();
                glScalef(1.0, 1.0, 0.1);
                gluSphere(qobj2, 4.0, 20, 20);
                glPushMatrix();
                    glTranslatef(0.0, 0.0, 60.0);
                    gluSphere(qobj2, 2.0, 20, 20);
                glPopMatrix();
            glPopMatrix();
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawStick(State* state){
    state->stickList = glGenLists(1);
    glNewList(state->stickList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[4]);

        gluSphere(qobj2, 1.5, 20, 20);
        glPushMatrix();
            glRotatef(90, 1.0, 0.0, 0.0);
            gluCylinder(qobj2, 0.5, 0.4, 30, 40, 16);
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}   

void drawWall(State* state){
    state->wallList = glGenLists(1);
    glNewList(state->wallList, GL_COMPILE);
        glColor3f(1,1,1);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[6]);
        glBegin(GL_QUADS);
            glNormal3f(0.0, 0.0, 1.0);

            glTexCoord2f(0,0);
            glVertex3f(-50.0, 0.0, 0.0);
            glTexCoord2f(1,0);
            glVertex3f(50.0, 0.0, 0.0);
            glTexCoord2f(1,1);
            glVertex3f(50.0, 10.0, 0.0);
            glTexCoord2f(0,1);
            glVertex3f(-50.0, 10.0, 0.0);
        glEnd();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawFloor(State* state){
    state->floorList = glGenLists(1);
    glNewList(state->floorList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[7]);
        glBegin(GL_QUADS);
            glNormal3f(0.0, 1.0, 0.0);

            glTexCoord2f(0,0);
            glVertex3f(-50.0, 0.0, -50.0);
            glTexCoord2f(1,0);
            glVertex3f(50.0, 0.0, -50.0);
            glTexCoord2f(1,1);
            glVertex3f(50.0, 0.0, 50.0);
            glTexCoord2f(0,1);
            glVertex3f(-50.0, 0.0, 50.0);
        glEnd();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawCeiling(State* state){
    state->ceilingList = glGenLists(1);
    glNewList(state->ceilingList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[8]);
        glBegin(GL_QUADS);
            glNormal3f(0.0, -1.0, 0.0);

            glTexCoord2f(0,0);
            glVertex3f(-50.0, 10.0, -50.0);
            glTexCoord2f(1,0);
            glVertex3f(50.0, 10.0, -50.0);
            glTexCoord2f(1,1);
            glVertex3f(50.0, 10.0, 50.0);
            glTexCoord2f(0,1);
            glVertex3f(-50.0, 10.0, 50.0);
        glEnd();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawTable(State* state){
    glPushMatrix();
        glScalef(0.5, 0.5, 0.5);
        glTranslatef(0.0, 5.0, 0.0);

        //Four legs
        glPushMatrix();
            glTranslatef(-10.0, 1.0, -10.0);
            glCallList(state->tableLegList);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(-10.0, 1.0, 10.0);
            glCallList(state->tableLegList);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(10.0, 1.0, 10.0);
            glCallList(state->tableLegList);
        glPopMatrix();
        glPushMatrix();
            glTranslatef(10.0, 1.0, -10.0);
            glCallList(state->tableLegList);
        glPopMatrix();

        //Table top
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPEAT);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glTranslatef(0.0, 3.0, 0.0);
        glScalef(24.0, -0.3, 20.0);
        gluSphere(qobj2, 1.0, 20, 20);

        glDisable(GL_TEXTURE_2D);
    glPopMatrix();
}

void drawTableLeg(State* state){
    state->tableLegList = glGenLists(1);
    glNewList(state->tableLegList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        gluSphere(qobj2, 2.0, 20, 20);
        glPushMatrix();
            glRotatef(90, 1.0, 0.0, 0.0);
            gluCylinder(qobj2, 1, 0.8, 6, 40, 16);
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawScene(State* state){
    
    //Drawing the wall
    glPushMatrix();
        glScalef(1.0, 2.0, 1.0);
        glPushMatrix();
            glTranslatef(0.0, 0.0, -50.0);
            glCallList(state->wallList);
            glTranslatef(-30.0, 0.0, 100.0);
            glPushMatrix();
                glScalef(0.40, 1.0, -1.0);
                glCallList(state->wallList);
            glPopMatrix();
            glTranslatef(60, 0.0, 0.0);
            glScalef(0.40, 1.0, -1.0);
            glCallList(state->wallList);
        glPopMatrix();
        glPushMatrix();
        //Drawing the door
            glTranslatef(state->doorOpen, 5.0, 50);
            glScalef(10, 5.0, 0.1);
            drawOpenCuboid(brown, black, true, 13);
        glPopMatrix();
        glPushMatrix();
            glRotatef(90, 0.0, 1.0, 0.0);
            glPushMatrix();
                glTranslatef(0.0, 0.0, -50.0);
                glCallList(state->wallList);
                glTranslatef(0.0, 0.0, 100.0);
                glScalef(1.0, 1.0, -1.0);
                glCallList(state->wallList);
            glPopMatrix();
        glPopMatrix();
        glCallList(state->floorList);
        glCallList(state->ceilingList);
    glPopMatrix();
        //gluSphere(qobj, 20, 20, 20);
    glPushMatrix();
        glTranslatef(5, 4.2, 5);
        drawLamp(state);
    glPopMatrix();

    glPushMatrix();
        glTranslatef(0, 19.8, 0);
        drawWallLight(state);
    glPopMatrix();
    //Human model and box
    glPushMatrix();
        glTranslatef(0.0, 5.5, 0.0);
        glScalef(0.15, 0.15, 0.15);
        glTranslatef(0.0, -4.0, 0.0);
        glPushMatrix();
            glScalef(9.0, 9.0, 9.0);
            //glScalef(9.0, 9.0, 9.0);
            drawBox(state);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0.0, 4.0, 0.0);
            drawPerson(state);
        glPopMatrix();
    glPopMatrix();


    //Chair
    glPushMatrix();
        glTranslatef(5.0, 3.0, 11.0);
        glScalef(0.5, 0.5, -0.5);
        drawChair(state);
        glTranslatef(-15.0, 0.0, 0.0);
        drawChair(state);
        glScalef(1.0, 1.0, -1.0);
        glTranslatef(0.0, 0.0, -44.0);
        drawChair(state);
        glTranslatef(15.0, 0.0, 0.0);
        drawChair(state);
    glPopMatrix();
    
    //Drawing the spheres
    //path->drawControlPoints();
    
    //Drawing the book shelf
    glPushMatrix();
        glTranslatef(0.0, 9.0, -40.0);
        glScalef(0.8, 0.8, 0.8);
        glCallList(state->bookShelfList);
    glPopMatrix();

    //Drawing the shoe shelf
    glPushMatrix();
        glTranslatef(-45.0, 2.5, 0);
        glRotatef(90, 0.0, 1.0, 0.0);
        glScalef(0.3, 0.2, 0.3);
        glCallList(state->shoeShelfList);
    glPopMatrix();

    //Potraits
    glPushMatrix();
        glRotatef(90, 0.0, 1.0, 0.0);
        glTranslatef(20.0, 10.0, -48.0);
        glScalef(5.0, 5.0, 1.0);
        glScalef(0.8, 1.0, 1.0);
        drawPotrait(state, 11);
    glPopMatrix();

    //Table
    drawTable(state);

    //Wall Clock
    glPushMatrix();
        glTranslatef(30.0, 13.0, -45.0);
        glScalef(2.0, 2.0, 2.0);
        glCallList(state->clockList);
    glPopMatrix();

    //Drawing fan
    glPushMatrix();
        glTranslatef(-45.0, 6.0, -45.0);
        glPushMatrix();
            glRotatef(45, 0.0, 1.0, 0.0);
            glCallList(state->fanList);
        glPopMatrix();
        glTranslatef(90.0, 0.0, 0.0);
        glRotatef(-45, 0.0, 1.0, 0.0);
        glCallList(state->fanList);
    glPopMatrix();

}

void drawChair(State* state){
    glColor3f(1,1,1);
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glBindTexture(GL_TEXTURE_2D, texture[0]);

    //Drawing the chair
    glPushMatrix();
        //Chair top
        glPushMatrix();
            glScalef(0.8, -0.08, 0.8);
            gluSphere(qobj2, 5.0, 20, 20);
        glPopMatrix();
        //Chair Legs
        glPushMatrix();
            glScalef(0.8, 1.0, 0.8);
            glRotatef(45, 0.0, 1.0, 0.0);
            glCallList(state->chairLegList);
            glRotatef(90, 0.0, 1.0, 0.0);
            glCallList(state->chairLegList);
            glRotatef(90, 0.0, 1.0, 0.0);
            glCallList(state->chairLegList);
            glRotatef(90, 0.0, 1.0, 0.0);
            glCallList(state->chairLegList);
        glPopMatrix();
        //Chair back
        glPushMatrix();
            glRotatef(90, 0.0, 1.0, 0.0);
            glTranslatef(3.0, 6.8, -2.0);
            glPushMatrix();
                glCallList(state->chairBackList);
                glTranslatef(0.0, 0.0, 1.0);
                glCallList(state->chairBackList);
                glTranslatef(0.0, 0.0, 1.0);
                glCallList(state->chairBackList);
                glTranslatef(0.0, 0.0, 1.0);
                glCallList(state->chairBackList);
                glTranslatef(0.0, 0.0, 1.0);
                glCallList(state->chairBackList);
            glPopMatrix();
            glTranslatef(0.0, 0.8, 5.0);
            glRotatef(90, 1.0, 0.0, 0.0);
            glCallList(state->chairBackList);
        glPopMatrix();
    glPopMatrix();
    glDisable(GL_TEXTURE_2D);
}

void drawChairLeg(State* state){
    state->chairLegList = glGenLists(1);
    glNewList(state->chairLegList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glPushMatrix();
            glTranslatef(0.0, 0.1, -3.5);
            glRotatef(10, 1.0, 0.0, 0.0);
            glRotatef(90, 1.0, 0.0, 0.0);
            gluCylinder(qobj2, 0.8, 0.7, 6, 40, 16);
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawChairBack(State* state){
    state->chairBackList = glGenLists(1);
    glNewList(state->chairBackList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);

        glPushMatrix();
            glScalef(0.3, 1.0, 0.3);
            glRotatef(90, 1.0, 0.0, 0.0);
            gluCylinder(qobj2, 1.0, 1.0, 6, 40, 16);
            gluSphere(qobj2, 1.0, 20, 20);
            glTranslatef(0.0, 0.0, 6.0);
            gluSphere(qobj2, 1.0, 20, 20);
        glPopMatrix();
        glDisable(GL_TEXTURE_2D);
    glEndList();
}

void drawBookShelf(State* state){
    state->bookShelfList = glGenLists(1);
    glNewList(state->bookShelfList, GL_COMPILE);
        glPushMatrix();
            glPushMatrix();
                glScalef(10.0, 10.0, 5.0);
                glEnable(GL_TEXTURE_2D);
                glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                glBindTexture(GL_TEXTURE_2D, texture[9]);
                glBegin(GL_QUADS);
                    glNormal3f(0.0, 0.0, 1.0);

                    glTexCoord2f(0,0);
                    glVertex3f(-1.0, -1.0, 1.0);
                    glTexCoord2f(1,0);
                    glVertex3f(1.0, -1.0, 1.0);
                    glTexCoord2f(1,1);
                    glVertex3f(1.0, 1.0, 1.0);
                    glTexCoord2f(0,1);
                    glVertex3f(-1.0, 1.0, 1.0);
                glEnd();
                glRotatef(90, 1.0, 0.0, 0.0);
                drawOpenCuboid(brown, black, true, 1);  
                glDisable(GL_TEXTURE_2D);
            glPopMatrix();
            //Enabling Texture maps
            glEnable(GL_TEXTURE_2D);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glBindTexture(GL_TEXTURE_2D, texture[0]);
            glTranslatef(6.0, -10.0, 0.0);
            gluSphere(qobj2, 2.0, 20, 20);
            glTranslatef(-12.0, 0.0, 0.0);
            gluSphere(qobj2, 2.0, 20, 20);
            glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    glEndList();
}

void drawShoeShelf(State* state){
    state->shoeShelfList = glGenLists(1);
    glNewList(state->shoeShelfList, GL_COMPILE);
        glPushMatrix();
            glPushMatrix();
                glScalef(10.0, 10.0, 5.0);
                glEnable(GL_TEXTURE_2D);
                glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
                glBindTexture(GL_TEXTURE_2D, texture[10]);
                glBegin(GL_QUADS);
                    glNormal3f(0.0, 0.0, 1.0);

                    glTexCoord2f(1,1);
                    glVertex3f(-1.0, -1.0, 1.0);
                    glTexCoord2f(0,1);
                    glVertex3f(1.0, -1.0, 1.0);
                    glTexCoord2f(0,0);
                    glVertex3f(1.0, 1.0, 1.0);
                    glTexCoord2f(1,0);
                    glVertex3f(-1.0, 1.0, 1.0);
                glEnd();
                glRotatef(90, 1.0, 0.0, 0.0);
                drawOpenCuboid(brown, black, true, 1);  
                glDisable(GL_TEXTURE_2D);
            glPopMatrix();
            //Enabling Texture maps
            glEnable(GL_TEXTURE_2D);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glBindTexture(GL_TEXTURE_2D, texture[0]);
            glTranslatef(6.0, -10.0, 0.0);
            gluSphere(qobj2, 2.0, 20, 20);
            glTranslatef(-12.0, 0.0, 0.0);
            gluSphere(qobj2, 2.0, 20, 20);
            glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    glEndList();
}

void drawPotrait(State* state, int textureInd){
    glPushMatrix();
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[textureInd]);
        glBegin(GL_QUADS);
            glNormal3f(0.0, 0.0, 1.0);

            glTexCoord2f(0,0);
            glVertex3f(-1.0, -1.0, 0.0);
            glTexCoord2f(1,0);
            glVertex3f(1.0, -1.0, 0.0);
            glTexCoord2f(1,1);
            glVertex3f(1.0, 1.0, 0.0);
            glTexCoord2f(0,1);
            glVertex3f(-1.0, 1.0, 0.0);
        glEnd();
        glDisable(GL_TEXTURE_2D);

        //Binding wood
        //glBindTexture(GL_TEXTURE_2D, texture[0]);
        glPushMatrix();
            glPushMatrix();
                glTranslatef(-1.1, 0.0, 0.02);
                glRotatef(-90, 1.0, 0.0, 0.0);
                glScalef(0.1, 0.02, 1.0);
                drawOpenCuboid(brown, black, true, 0);
                glTranslatef(22.0, 0.0, 0.0);
                drawOpenCuboid(brown, black, true, 0);
            glPopMatrix();
            glPushMatrix();
                glRotatef(90, 0.0, 0.0, 1.0);
                glTranslatef(-1.1, 0.0, 0.02);
                glRotatef(-90, 1.0, 0.0, 0.0);
                glScalef(0.1, 0.02, 1.2);
                drawOpenCuboid(brown, black, true, 0);
                glTranslatef(22.0, 0.0, 0.0);
                drawOpenCuboid(brown, black, true, 0);
            glPopMatrix();
        glPopMatrix();
    glPopMatrix();
}

void drawClock(State* state){
    state->clockList = glGenLists(1);
    glNewList(state->clockList, GL_COMPILE);
        glPushMatrix();
            glPushMatrix();
                glScalef(1.0, 1.0, 0.05);
                drawOpenCuboid(brown, black, true, 12);
            glPopMatrix();
            glEnable(GL_TEXTURE_2D);
            glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
            glBindTexture(GL_TEXTURE_2D, texture[0]);
            glPushMatrix();
                glTranslatef(0.0, 0.0, -0.2);
                glScalef(0.8, 1.0, 0.1);
                gluSphere(qobj2, 2.0, 20, 20);
                //drawOpenCuboid(brown, black, true, 0);
            glPopMatrix();
            //Pendulum
            glPushMatrix();
                glTranslatef(0.0, -1.4, 0.0);
                glRotatef(state->pendAng, 0.0, 0.0, 1.0);
                glPushMatrix();
                    glTranslatef(0.0, -3.0, 0.0);
                    glScalef(1.0, 1.0, 0.5);
                    gluSphere(qobj2, 0.4, 20, 20);
                glPopMatrix();
                glScalef(0.1, 0.5, -0.1);
                glRotatef(90, 1.0, 0.0, 0.0);
                gluCylinder(qobj2, 1.0, 1.0, 6, 40, 16);
                gluSphere(qobj2, 1.0, 20, 20);
            glPopMatrix();
            glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    glEndList();

}

void drawFan(State* state){
    state->fanList = glGenLists(1);
    glNewList(state->fanList, GL_COMPILE);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        glPushMatrix();
            glPushMatrix();
                glTranslatef(0.0, -6.0, 0.0);
                glScalef(1.0, 0.1, 1.0);
                gluSphere(qobj2, 2, 20, 20);
                glScalef(1.0, -10.0, 1.0);
                glRotatef(90, 1.0, 0.0, 0.0);
                gluCylinder(qobj2, 0.3, 0.3, 6, 40, 16);
            glPopMatrix();
            glPushMatrix();
                gluSphere(qobj2, 0.5, 20, 20);
                glBindTexture(GL_TEXTURE_2D, texture[4]);
                glTranslatef(0.0, 0.0, 0.5);
                glRotatef(state->fanAng, 0.0, 0.0, 1.0);
                glPushMatrix();
                    glTranslatef(2.0, 0.0, 0.0);
                    glScalef(2.0, 0.5, 0.10);
                    gluSphere(qobj2, 1.0, 20, 20);
                glPopMatrix();

                glPushMatrix();
                    glRotatef(120, 0.0, 0.0, 1.0);
                    glTranslatef(2.0, 0.0, 0.0);
                    glScalef(2.0, 0.5, 0.10);
                    gluSphere(qobj2, 1.0, 20, 20);
                glPopMatrix();

                glPushMatrix();
                    glRotatef(240, 0.0, 0.0, 1.0);
                    glTranslatef(2.0, 0.0, 0.0);
                    glScalef(2.0, 0.5, 0.10);
                    gluSphere(qobj2, 1.0, 20, 20);
                glPopMatrix();
            glPopMatrix();
            glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    glEndList();
}

void drawLamp(State* state){
    glPushMatrix();
        glColor3f(0.6, 0.6, 0.6);
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, texture[4]);

        glPushMatrix();
            GLfloat emmision[] = {1.0, 1.0, 0.2, 1.0};
            GLfloat emmisiondef[] = {0, 0, 0, 1.0};
            if(state->lamp == 1)
                glMaterialfv(GL_FRONT, GL_EMISSION, emmision);
            glDisable(GL_TEXTURE_2D);
            glTranslatef(0, 5, 0);
            glScalef(1, -1, 1);
            glRotatef(270, 1, 0, 0);
            gluCylinder(qobj2, 1.2, 2.0, 3, 40, 16);
            glEnable(GL_TEXTURE_2D);
            glMaterialfv(GL_FRONT, GL_EMISSION, emmisiondef);
        glPopMatrix();

        glPushMatrix();
            glTranslatef(0, 2, 0);
            glScalef(1, -1, 1);
            glRotatef(270, 1, 0, 0);
            gluCylinder(qobj2, 0.2, 0.2, 2, 40, 16);
        glPopMatrix();

        glPushMatrix();
            glScalef(1, 0.2, 1);
            gluSphere(qobj2, 1.0, 20, 20);
        glPopMatrix();
    glPopMatrix();
}

void drawWallLight(State* state){
    glPushMatrix();
        glColor3f(0.6, 0.6, 0.6);
        GLfloat emmision[] = {1.0, 1.0, 1.0, 1.0};
        GLfloat emmisiondef[] = {0, 0, 0, 1.0};
        if(state->light == 1)
            glMaterialfv(GL_FRONT, GL_EMISSION, emmision);
        glDisable(GL_TEXTURE_2D);
        glScalef(1,0.2,1);
        drawOpenCuboid(white, white, 0,  0);
        glEnable(GL_TEXTURE_2D);
        glMaterialfv(GL_FRONT, GL_EMISSION, emmisiondef);
    glPopMatrix();
}

void drawPlane(GLfloat z){
    glBegin(GL_QUADS);
            glNormal3f(0.0, 0.0, -1);

            glTexCoord2f(0,0);
            glVertex3f(-50.0, 0.0, z);
            glTexCoord2f(1,0);
            glVertex3f(50.0, 0.0, z);
            glTexCoord2f(1,1);
            glVertex3f(50.0, 20.0, z);
            glTexCoord2f(0,1);
            glVertex3f(-50.0, 20.0, z);
    glEnd();
}

//Function to enable all the display Lists
void initializeLists(State* state){
    drawBoxPart(state);
    drawBoxPart2(state);
    drawLimb(state);
    drawJoint(state);
    drawTorso(state);
    drawNeck(state);
    drawHead(state);
    drawHat(state);
    drawStick(state);
    drawFloor(state);
    drawWall(state);
    drawCeiling(state);
    drawTableLeg(state);
    drawChairLeg(state);
    drawChairBack(state);
    drawBookShelf(state);
    drawShoeShelf(state);
    drawClock(state);
    drawFan(state);
}

