#include "Path.h"

Point::Point(float X, float Y, float Z){
    x = X;
    y = Y;
    z = Z;
}

Path::Path(){
    cPts.push_back(Point(0,7,100));
    cPts.push_back(Point(0, 7, 5));
}

Path::Path(std::vector<Point> controlPoints){
    cPts = controlPoints;
}

void Path::clear(){
    cPts.clear();
    cPts.push_back(Point(0,7,100));
    cPts.push_back(Point(0, 7, 5));
    //getSamplePoints(0.01);
}


void Path::addPoint(Point newPt){
    cPts.pop_back();
    cPts.push_back(newPt);
    cPts.push_back(Point(0, 7, 5));
    //getSamplePoints(0.01);
}

void Path::undo(){
    cPts.pop_back();
    if(cPts.size() > 1)
        cPts.pop_back();
    cPts.push_back(Point(0, 7, 5));
    //getSamplePoints(0.01);
}

void Path::drawControlPoints(){
    for (int i = 0; i < cPts.size() ; i++){
        glPushMatrix();
            glColor3f(0.6, 0.1, 0.1);
            glTranslatef(cPts[i].x, cPts[i].y, cPts[i].z);
            gluSphere(qobj2, 1, 10, 10);
        glPopMatrix();
    }
}

void Path::drawPoints(std::vector<Point> points){
    //glColor3f(ptColor->R, ptColor->G, ptColor->B);
    glPushMatrix();
    glLineWidth(3);
    for (int i = 0; i < points.size()-1 ; i++){
            //glTranslatef(points[i].x, points[i].y, points[i].z);
            //gluSphere(qobj2, 0.25, 10, 10);
            glBegin(GL_LINES);
                glVertex3f(points[i].x, points[i].y, points[i].z);
                glVertex3f(points[i+1].x, points[i+1].y, points[i+1].z);
            glEnd();
        
    }
    glPopMatrix();
}

void Path::printPoints(){
    for (int i = 0; i < cPts.size() ; i++)
        printf("Vertex (x, y, z) : %f %f %f\n", cPts[i].x, cPts[i].y, cPts[i].z);
}

std::vector<Point> Path::recurseLinear(std::vector<Point> curPts, float u){
    if(u > 1){
        std::cout << "Value of u must be in [0,1]\n";    
    }

    std::vector<Point> nextPts;
    for(int i = 0; i < curPts.size() - 1 ; i++){
        //Replaced later by operational overloads
        Point nextPt;
        nextPt.x = curPts[i].x * u + curPts[i+1].x * (1-u);
        nextPt.y = curPts[i].y * u + curPts[i+1].y * (1-u);
        nextPt.z = curPts[i].z * u + curPts[i+1].z * (1-u);
        nextPts.push_back(nextPt); 
    }
    return nextPts;
}

void Path::getSamplePoints(float stepSize){
    std::vector<Point> curvePts;
    for(float curParam = 1; curParam >= 0; curParam-=stepSize){
        //std::cout << curParam << std::endl;
        std::vector<Point> samplePts;
        samplePts = recurseLinear(cPts, curParam);
        while(samplePts.size() != 1){
            samplePts = recurseLinear(samplePts, curParam);
        }
        curvePts.push_back(samplePts[0]);
    }
    //drawPoints(curvePts);
    lastSamplePts = curvePts;
}
