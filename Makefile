SOILdir = SOIL
SOIL = SOIL/lib/libSOIL.a

all: testRun

$(SOIL):
	make -f Makefile -C $(SOILdir)/projects/makefile

testRun: test.o drawFunctions.o supportClass.o state.o Path.o pose.o $(SOIL)
	g++ test.o drawFunctions.o supportClass.o state.o Path.o pose.o $(SOIL) -lglut -lGL -lGLU -o testRun

test.o: test.cpp 
	g++ -c test.cpp -lglut -lGL -lGLU

drawFunctions.o: drawFunctions.cpp
	g++ -c drawFunctions.cpp -lglut -lGL -lGLU

supportClass.o: supportClass.cpp
	g++ -c supportClass.cpp -lglut -lGL -lGLU

state.o: state.cpp
	g++ -c state.cpp -lglut -lGL -lGLU

guiFunctions.o: guiFunctions.cpp
	g++ -c guiFunctions.cpp -lglut -lGL -lGLU

Path.o: Path.cpp
	g++ -c Path.cpp -lglut -lGL -lGU

pose.o: pose.cpp
	g++ -c pose.cpp -lglut -lGL -lGU

basic:
	./testRun

clean:
	rm -rf *.o
	rm -rf testRun*
	make -f Makefile -C $(SOILdir)/projects/makefile clean
