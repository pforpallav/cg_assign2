#include "supportClass.h"

#ifndef COLOR_DEFN
#define COLOR_DEFN
Color* brown = new Color(0.59, 0.30, 0.0);
Color* dbrown = new Color(0.49, 0.25, 0.0);
Color* white = new Color(1.0, 1.0, 1.0);
Color* black = new Color(0.0, 0.0, 0.0);
Color* red = new Color(1.0, 0.0, 0.0);
Color* dred = new Color(0.6, 0.0, 0.0);
Color* green = new Color(0.0, 1.0, 0.0);
Color* dgreen = new Color(0.0, 0.6, 0.0);
Color* blue = new Color(0.0, 0.0, 1.0);
Color* yellow = new Color(0.0, 1.0, 1.0);
Color* gray = new Color(0.2, 0.2, 0.2);

#endif
///////////////////////////////////////


