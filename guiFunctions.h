#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "state.h"

#ifndef GUI_FUNC
#define GUI_FUNC

//Variables
double rotate_y = 0;
double rotate_x = 0;

//Methods
void specialKeys(int, int, int);
void keys(unsigned char, int, int);

#endif
