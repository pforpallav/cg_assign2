#include "pose.h"

//Print Pose
void Pose::printPose(){
    std::cout << headTilt << "\t" << headBend << "\t" << headTurn << "\t" << lArmFront << "\t" << lArmSide << "\t" << lArmRot << "\t" << rArmFront << "\t" << rArmSide << "\t" << rArmRot << "\t" << rElbowAng << "\t" << lElbowAng << "\t" << rLegFront << "\t" << rLegSide << "\t" << rLegRot << "\t" << lLegFront << "\t" << lLegSide << "\t" << lLegRot << "\t" << rKneeAng << "\t" << lKneeAng << "\t" << torsoBend << "\t" << torsoTurn << "\t" << torsoTilt << std::endl;
}

//Operator +
Pose Pose::operator +(Pose oPose){
    Pose sPose = Pose();
    //Head
    sPose.headTilt = oPose.headTilt + headTilt;
    sPose.headBend = oPose.headBend + headBend;
    sPose.headTurn = oPose.headTurn + headTurn;

    //Left Arm Joint
    sPose.lArmFront = oPose.lArmFront + lArmFront;
    sPose.lArmSide = oPose.lArmSide + lArmSide; //side angle
    sPose.lArmRot = oPose.lArmRot + lArmRot; //Rotation of the arm

    //Right Arm Joint
    sPose.rArmFront = oPose.rArmFront + rArmFront; //front and back angle
    sPose.rArmSide = oPose.rArmSide + rArmSide; //side angle
    sPose.rArmRot = oPose.rArmRot + rArmRot; //Rotation of the arm

    //Elbow angle
    sPose.rElbowAng = oPose.rElbowAng + rElbowAng;
    sPose.lElbowAng = oPose.lElbowAng + lElbowAng;

    //Right leg joint
    sPose.rLegFront = oPose.rLegFront + rLegFront;
    sPose.rLegSide = oPose.rLegSide + rLegSide;
    sPose.rLegRot = oPose.rLegRot + rLegRot;

    //Left leg joint
    sPose.lLegFront = oPose.lLegFront + lLegFront;
    sPose.lLegSide = oPose.lLegSide + lLegSide;
    sPose.lLegRot = oPose.lLegRot + lLegRot;

    //Knee angle
    sPose.rKneeAng = oPose.rKneeAng + rKneeAng;
    sPose.lKneeAng = oPose.lKneeAng + lKneeAng;

    //torso angle
    sPose.torsoBend = oPose.torsoBend + torsoBend; 
    sPose.torsoTurn = oPose.torsoTurn + torsoTurn;
    sPose.torsoTilt = oPose.torsoTilt + torsoTilt;

    //returning
    return sPose;
}

Pose Pose::operator * (float scalar){
    Pose sPose = Pose();
    //Head
    sPose.headTilt = scalar * headTilt;
    sPose.headBend = scalar * headBend;
    sPose.headTurn = scalar * headTurn;

    //Left Arm Joint
    sPose.lArmFront = scalar * lArmFront;
    sPose.lArmSide = scalar * lArmSide; //side angle
    sPose.lArmRot = scalar * lArmRot; //Rotation of the arm

    //Right Arm Joint
    sPose.rArmFront = scalar * rArmFront; //front and back angle
    sPose.rArmSide = scalar * rArmSide; //side angle
    sPose.rArmRot = scalar * rArmRot; //Rotation of the arm

    //Elbow angle
    sPose.rElbowAng = scalar * rElbowAng;
    sPose.lElbowAng = scalar * lElbowAng;

    //Right leg joint
    sPose.rLegFront = scalar * rLegFront;
    sPose.rLegSide = scalar * rLegSide;
    sPose.rLegRot = scalar * rLegRot;

    //Left leg joint
    sPose.lLegFront = scalar * lLegFront;
    sPose.lLegSide = scalar * lLegSide;
    sPose.lLegRot = scalar * lLegRot;

    //Knee angle
    sPose.rKneeAng = scalar * rKneeAng;
    sPose.lKneeAng = scalar * lKneeAng;

    //torso angle
    sPose.torsoBend = scalar * torsoBend; 
    sPose.torsoTurn = scalar * torsoTurn;
    sPose.torsoTilt = scalar * torsoTilt;

    return sPose;
}

